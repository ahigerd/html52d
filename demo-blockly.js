import config from './config.js';
import BlocklyEditor, { makeBlocklyPrefabClass } from './html52d/BlocklyEditor.js';
/* global assets:false, camera:false, engine:false */

export default () => {
  config.noAutoStart = true;
  camera.setXY(0, 0);
  //camera.setScale(2);

  assets.prefabs.Hero = makeBlocklyPrefabClass('Hero');
  assets.prefabs.Hero.config = {
    animateHitboxes: false,
    defaultIsAnimating: true,
    defaultAnimationName: 'right',
    hitbox: [-.17, 0, .15, .5, 0xFFFFFFFF],
    animations: {
      left: [
        [assets.images.hero, 16, 16, -16, 16],
        [assets.images.hero, 32, 16, -16, 16],
        [assets.images.hero, 48, 16, -16, 16],
        [assets.images.hero, 64, 16, -16, 16],
        250.0,
      ],
      right: [
        [assets.images.hero, 16, 16, 16, 16],
        [assets.images.hero, 32, 16, 16, 16],
        [assets.images.hero, 48, 16, 16, 16],
        [assets.images.hero, 64, 16, 16, 16],
        250.0,
      ],
      runLeft: [
        [assets.images.hero, 16, 32, -16, 16],
        [assets.images.hero, 32, 32, -16, 16],
        [assets.images.hero, 48, 32, -16, 16],
        [assets.images.hero, 64, 32, -16, 16],
        125.0,
      ],
      runRight: [
        [assets.images.hero, 16, 32, 16, 16],
        [assets.images.hero, 32, 32, 16, 16],
        [assets.images.hero, 48, 32, 16, 16],
        [assets.images.hero, 64, 32, 16, 16],
        125.0,
      ],
    },
  };

  const editor = new BlocklyEditor(assets, {
    toolboxPath: 'toolbox.xml',
    oneBasedIndex: false,
    scrollbars: true,
    allowNewPrefabs: true,
  });
  editor.appendTo(document.body);

  let lastCode = null;
  editor.addEventListener('load', () => {
    engine.start();
  });
};
