import config from './config.js';
import Sprite, { AnimationSequence, AnimationFrame } from './html52d/Sprite.js';
/* global commonRender, sprites */

export default () => {
  if (!config.TEST_SPRITES) {
    config.TEST_SPRITES = 200;
  }

  const prefab = {
    isTrigger: true,
    animateHitboxes: true,
    animations: {
      default: new AnimationSequence([
        new AnimationFrame(null, 0, 0, 32, 32),
      ]),
    },
    start(/*scene*/) {
      this.collisionCount = 0;
      this.dx = Math.random() - 0.5;
      this.dy = Math.random() - 0.5;
    },
    update(scene, ms) {
      if (config.running) {
        this.teleported = false;
        this.move(this.dx * ms / 150.0, this.dy * ms / 150.0);
        if (this.origin[0] > 22) {
          this._moved = false;
          this.origin[0] -= 24;
          this.teleported = true;
        } else if (this.origin[0] < -2) {
          this._moved = false;
          this.origin[0] += 24;
          this.teleported = true;
        }
        if (this.origin[1] > 18) {
          this._moved = false;
          this.origin[1] -= 20;
          this.teleported = true;
        } else if (this.origin[1] < -2) {
          this._moved = false;
          this.origin[1] += 20;
          this.teleported = true;
        }
      }
    },
    onCollisionEnter(/*other, coll*/) {
      this.collisionCount++;
    },
    onCollisionExit(/*other*/) {
      this.collisionCount--;
    },
    render(camera) {
      const level = 1.0 - 1.0 / (1.0 + this.collisionCount / 2);
      camera.layers[0].fillStyle = 'rgba(0, 0, 0, ' + level + ')';
      commonRender.call(this, camera, true);
    },
  };

  return function constructOne(index) {
    const sprite = new Sprite(prefab);
    sprite.index = index;
    sprite.origin.setXY((sprites.length % 20) * 0.9 + 1.5, Math.floor(sprites.length / 20) * 0.9 + 2.0);
    return sprite;
  };
};
