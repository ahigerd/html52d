import defineBlock from './defineBlock.js';
import { makePrefabsDropdown } from './prefab_new.js';

defineBlock({
  name: 'prefab_isinstance',
  init() {
    this.appendValueInput('OBJECT');
    this.appendDummyInput()
      .appendField('is based on')
      .appendField(new Blockly.FieldDropdown(makePrefabsDropdown()), 'PREFAB');
    this.setInputsInline(true);
    this.setOutput(true, 'Boolean');
    this.setColour(55);
  },
  render(block, { OBJECT, PREFAB }) {
    return `${OBJECT('INSTANCEOF')} instanceof assets.prefabs.${PREFAB}`;
  },
  precedence: 'INSTANCEOF',
});
