import defineBlock from './defineBlock.js';

defineBlock({
  name: 'math_point_var',
  init() {
    this.appendValueInput('P_X')
      .setCheck('Number')
      .appendField('Point(');
    this.appendValueInput('P_Y')
      .setCheck('Number')
      .appendField(',');
    this.appendDummyInput().appendField(')');
    this.setColour('#5b67a5');
    this.setOutput(true, 'Point');
  },
  render(block, { P_X, P_Y }) {
    return `new Point(${P_X('COMMA')}, ${P_Y('COMMA')})`;
  },
});
