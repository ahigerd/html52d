import './object_with.js';
import './object_this.js';
import './object_member.js';
import './object_member_set.js';
import './object_member_change.js';
import './object_this_member.js';
import './object_this_member_set.js';
import './object_this_member_change.js';
