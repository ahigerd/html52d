import defineBlock from './defineBlock.js';
import { buildUI } from '../html52d/UITools.js';
/* global Blockly:false, Input:false */

const dialogHtml = `
  <div id='keyDialog' data-type='UIDialog' tabindex='-1' data-on-keydown='keydown'>
    Press the key to check for...<br/>
    <i>Click anywhere to cancel.</i>
  </div>
`;

function makeFieldKeyClass(Field) {
  const { keyDialog } = buildUI({
    keydown(evt) {
      let key = Input.normalize[evt.key] || evt.key;
      keyDialog.resolve(key === ' ' ? 'Space' : key);
    },
  }, null, dialogHtml);
  keyDialog.addEventListener('open', keyDialog.focus.bind(keyDialog));
  keyDialog.addEventListener('click', keyDialog.hide.bind(keyDialog));

  return class FieldKey extends Field {
    constructor() {
      super('Space');
    }

    onMouseDown_(evt) {
      if (this.sourceBlock_.isInFlyout || keyDialog.isVisible) {
        // Don't pop open the dialog if it's still in the toolbox
        // Also don't pop it open if the dialog is already open
        return;
      }

      keyDialog.open().then(key => key && this.setValue(key));
    }
  };
}

let FieldKey = null;

defineBlock({
  name: 'input_key_down',
  init() {
    if (!FieldKey) {
      FieldKey = makeFieldKeyClass(Blockly.Field);
    }
    const keyField = new FieldKey();
    this.appendDummyInput()
      .appendField('is')
      .appendField(keyField, 'KEY')
      .appendField('pressed?');
    this.setOutput(true, 'Boolean');
    this.setColour(160);
  },
  render(block, { KEY }) {
    return `Input.keys[${JSON.stringify(KEY === 'Space' ? ' ' : KEY)}]`;
  },
  precedence: 'NONE',
  toolboxXml: '<block type="input_key_down"></block>',
});
