import defineBlock, { getThis } from './defineBlock.js';
import { getCurrentMembers, getMemberLabel } from './objectData.js';

defineBlock({
  name: 'object_this_member',
  init() {
    this.appendDummyInput()
      .appendField("this object's")
      .appendField(new Blockly.FieldDropdown(getCurrentMembers), 'MEMBER');
    this.setOutput(true);
    this.setColour(260);
  },
  render(block, { MEMBER }) {
    return `this.${MEMBER.split('::')[1]}`;
  },
  precedence: 'MEMBER',
  toolboxXml: (ws) => {
    if (!window.editor.selectedPrefab || window.editor.selectedFunction) {
      return '';
    }
    return '<block type="object_this_member" />' +
      getCurrentMembers().slice(3).map(m => `<sep gap="8" /><block type='object_this_member'><field name='MEMBER'>${m[1]}</field></block>`).join('');
  },
  customContextMenu(menuOptions) {
    const variable = this.getFieldValue('MEMBER');
    const [scope, name] = variable.split('::');
    if (this.isInFlyout) {
      if (!scope) {
        return;
      }
      menuOptions.unshift({
        enabled: true,
        text: `Rename member variable...`,
        callback: () => window.editor.renameMember(scope, name),
      });
      menuOptions.unshift({
        enabled: true,
        text: `Delete this object\'s '${name}' member variable`,
        callback: () => window.editor.deleteMember(scope, name),
      });
    } else {
      const label = getMemberLabel(variable);
      menuOptions.push({
        enabled: true,
        text: `Create 'set this object\'s ${label}'`,
        callback: Blockly.ContextMenu.callbackFactory(this, Blockly.Xml.textToDom(`<xml><block type='object_this_member_set'><field name='MEMBER'>${variable}</field></block></xml>`).firstChild),
      });
    }
  },
});
