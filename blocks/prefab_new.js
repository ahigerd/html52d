import defineBlock from './defineBlock.js';

export function makePrefabsDropdown() {
  return () => {
    const r = Object.keys(window.assets.prefabs).map(k => [k, k]);
    return r;
  };
}

defineBlock({
  name: 'prefab_new',
  init() {
    this.appendValueInput('ORIGIN').setCheck('Point')
      .appendField('new')
      .appendField(new Blockly.FieldDropdown(makePrefabsDropdown()), 'PREFAB')
      .appendField('at');
    this.setInputsInline(false);
    this.setOutput(true, 'Sprite');
    this.setColour(55);
  },
  render(block, { PREFAB, ORIGIN }) {
    return `new assets.prefabs.${PREFAB}(${ORIGIN('COMMA')})`;
  },
  precedence: 'NEW',
  toolboxXml: `<block type="prefab_new">
    <value name="ORIGIN"><shadow type="math_point_xy">
      <field name="P_X">0</field>
      <field name="P_Y">0</field>
    </shadow></value>
  </block>`,
});
