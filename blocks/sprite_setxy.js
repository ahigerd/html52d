import defineBlock, { getThis } from './defineBlock.js';

defineBlock({
  name: 'sprite_setxy',
  init() {
    this.appendValueInput('DEST')
      .setCheck('Point')
      .appendField('move to');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour('#c09000');
  },
  render(block, { DEST }) {
    return `${getThis(block)}.origin = ${DEST('ASSIGNMENT')};\n`;
  },
  toolboxXml: `<block type='sprite_setxy'>
    <value name="DEST"><shadow type="math_point_xy">
      <field name="P_X">0</field>
      <field name="P_Y">0</field>
    </shadow></value>
  </block>`,
});
