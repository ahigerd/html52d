import defineBlock, { getThis } from './defineBlock.js';

defineBlock({
  name: 'sprite_move_speed',
  init() {
    this.appendValueInput('P_X')
      .setCheck('Number')
      .appendField('move at speed');
    this.appendValueInput('P_Y')
      .setCheck('Number')
      .appendField(',');
    this.setInputsInline(true);
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour('#c09000');
  },
  render(block, { P_X, P_Y }) {
    return `${getThis(block)}.move(ms * .001 * ${P_X('MULTIPLICATION')}, ms * .001 * ${P_Y('MULTIPLICATION')});\n`;
  },
  toolboxXml: () => `<block type="sprite_move_speed">
    <value name="P_X">
      <shadow type="math_number">
        <field name="NUM">0</field>
      </shadow>
    </value>
    <value name="P_Y">
      <shadow type="math_number">
        <field name="NUM">0</field>
      </shadow>
    </value>
  </block>`,
});
