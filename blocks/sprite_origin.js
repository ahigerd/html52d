import defineBlock, { getThis } from './defineBlock.js';

defineBlock({
  name: 'sprite_origin',
  init() {
    this.appendDummyInput()
      .appendField('current position');
    this.setColour('#c09000');
    this.setOutput(true, 'Point');
  },
  render(block) {
    return `${getThis(block)}.origin`;
  },
  precedence: 'MEMBER',
});
