import defineBlock from './defineBlock.js';

defineBlock({
  name: 'function_return',
  init() {
    this.variableId = null;
    this.appendValueInput("VALUE")
      .appendField("Return the value");
    this.setPreviousStatement(true, 'default');
    this.setColour('#c0c000');
    this.setOnChange(() => {
      if (this.getChildren().length) {
        this.setWarningText(null);
      } else {
        this.setWarningText('A value is required.');
      }
    });
  },
  render(block, { VALUE }) {
    return `return ${VALUE('NONE')};\n`;
  },
  toolboxXml: () => window.editor.selectedFunction ? '<block type="function_return" />' : '',
});



