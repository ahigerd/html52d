import defineBlock from './defineBlock.js';
import eventTypes from './eventTypes.js';

defineBlock({
  name: 'function_entry',
  init() {
    this.eventLabel = null;
    this.eventArgs = [];
    this.functionName = '';

    this.appendDummyInput().appendField('function()');
    this.appendStatementInput('PARAMS').setCheck('fn_proto');
    this.setNextStatement(true, 'default');
    this.setColour('#c0c000');
    this.setDeletable(false);
    this.setEditable(true);
    this.setOnChange(event => {
      if (this.getReturns()) {
        let stmt = null, next = this;
        do {
          stmt = next;
          next = stmt.getNextBlock();
        } while (next);
        if (stmt.type !== 'function_return') {
          this.setWarningText('Not all paths return a value.');
          return;
        }
      }
      this.setWarningText(null);
    });
  },
  mutator: {
    getReturns() {
      return this.getDescendants().some(child => child.type === 'function_return');
    },
    getArguments() {
      if (this.eventLabel) {
        return this.eventArgs.map(arg => ({ name: arg, label: arg }));
      }
      const args = [];
      for (const child of this.getChildren(true)) {
        if (child.type === 'function_param' && !child.isShadow()) {
          args.push({
            name: child.getFieldValue("NAME"),
            label: child.getFieldValue("LABEL"),
          });
        }
      }
      return args;
    },
    getArgumentNames() {
      return this.getArguments().map(arg => arg.name);
    },
    getPreamble(useFunction = true) {
      let comment = this.getCommentText();
      if (comment) {
        comment = '/**\n' + Blockly.JavaScript.prefixLines(comment, ' * ') + ' */\n';
      } else {
        comment = '';
      }
      const signature = this.functionName + '(' + this.getArgumentNames().join(', ') + ')';
      if (useFunction) {
        return comment + 'function ' + signature;
      } else {
        return comment + signature;
      }
    },
    setFunctionName(name) {
      this.functionName = name;
      this.inputList[0].fieldRow[0].setText(`function ${name}()`);
    },
    setEvent(name) {
      const eventType = eventTypes[name];
      this.functionName = name;
      this.eventLabel = eventType.label;
      this.eventArgs = eventType.args || [];
      this.inputList[0].fieldRow[0].setText(eventType.label);
      this.removeInput('PARAMS', true);
    },
    domToMutation(xmlElement) {
      const name = xmlElement.getAttribute('name');
      const eventType = xmlElement.getAttribute('event');
      if (eventType) {
        this.setEvent(eventType);
      } else if (name) {
        this.setFunctionName(name);
      }
    },
    mutationToDom() {
      const mutation = document.createElement('mutation');
      if (this.eventLabel) {
        mutation.setAttribute('event', this.functionName);
      } else {
        mutation.setAttribute('name', this.functionName);
      }
      return mutation;
    },
  },
  render(block) {
    // Function blocks don't actually generate code of their own.
    // They provide an anchor point for code generation to start.
    return '';
  },
});

