import './sprite_move.js';
import './sprite_move_speed.js';
import './sprite_setxy.js';
import './sprite_origin.js';
import './sprite_origin_x.js';
import './sprite_origin_y.js';
import './sprite_set_animation.js';
import './sprite_play_animation.js';
