import defineBlock from './defineBlock.js';
import { getGlobalVariables } from './objectData.js';

defineBlock({
  name: 'global_variable_change',
  init() {
    this.appendValueInput('VALUE')
      .appendField('change global')
      .appendField(new Blockly.FieldDropdown(getGlobalVariables), 'VAR')
      .appendField('by');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour('#A04040');
  },
  render(block, { VAR, VALUE }) {
    return `window.${VAR} += ${VALUE('ASSIGNMENT')};\n`;
  },
  toolboxXml: () => !window.editor.globalScope.variables.length ? '' :
    '<sep gap="8" /><block type="global_variable_change"><value name="VALUE"><shadow type="math_number"><field name="NUM">1</field></shadow></value></block>',
});


