export default {
  'run': {
    name: 'run',
    label: 'When the game starts',
  },
  'start': {
    prefab: true,
    name: 'start',
    label: 'When the sprite is created',
    args: ['scene'],
  },
  'update': {
    prefab: true,
    name: 'update',
    label: 'When the game updates',
    args: ['scene', 'ms'],
  },
};
