import defineBlock, { getThis } from './defineBlock.js';
import { getCurrentMembers } from './objectData.js';

defineBlock({
  name: 'object_this_member_change',
  init() {
    this.appendValueInput('VALUE')
      .appendField("change this object's")
      .appendField(new Blockly.FieldDropdown(getCurrentMembers), 'MEMBER')
      .appendField('by');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour(260);
  },
  render(block, { MEMBER, VALUE }) {
    return `this.${MEMBER.split('::')[1]} += ${VALUE('ASSIGNMENT')};\n`;
  },
  toolboxXml: () => window.editor.selectedPrefab && !window.editor.selectedFunction ?
    '<sep gap="8" /><block type="object_this_member_change"><value name="VALUE"><shadow type="math_number"><field name="NUM">1</field></shadow></value></block>' : '',
});

