import defineBlock from './defineBlock.js';

defineBlock({
  name: 'object_this',
  init() {
    this.appendDummyInput().appendField('this sprite');
    this.setColour(260);
    this.setOutput(true, 'Sprite');
  },
  render(block) {
    return 'this';
  },
  precedence: 'ATOMIC',
  toolboxXml: () => window.editor.selectedPrefab && !window.editor.selectedFunction ? '<block type="object_this" />' : '',
});

