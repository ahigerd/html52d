/* global Blockly:false */

// TODO: find the rest of these values, or find symbolic constants in Blockly
const CONNECTION_VALUE = 1;
const CONNECTION_NEXT = 3;

export const ToolboxXml = {};
export const pendingBlocks = [];

function getFieldValues(block) {
  const fields = {};
  for (const input of block.inputList) {
    if (input.name) {
      if (input.connection && input.connection.type == CONNECTION_NEXT) {
        // Child statements. 
        const targetBlock = input.connection && input.connection.targetBlock();
        fields[input.name] = () => Blockly.JavaScript.blockToCode(targetBlock, input.name);
      } else if (input.connection && input.connection.type == CONNECTION_VALUE) {
        // Value inputs.
        fields[input.name] = precedence => Blockly.JavaScript.valueToCode(block, input.name, typeof precedence === 'string' ? Blockly.JavaScript[`ORDER_${precedence}`] : precedence) || 'null';
      }
    }
    for (const field of input.fieldRow) {
      if (field.name) {
        fields[field.name] = field.getValue();
      }
    }
  }
  return fields;
}


export function addPendingBlocks() {
  const blocks = [...pendingBlocks];
  pendingBlocks.splice(0, pendingBlocks.length);
  for (const config of blocks) {
    if (typeof config === 'function') {
      config();
    } else {
      defineBlock(config);
    }
  }
}

export function getThis(block) {
  const parent = block.getSurroundParent();
  if (!parent) {
    return 'this';
  }
  if (parent.type === 'object_with') {
    return Blockly.JavaScript.valueToCode(parent, 'THIS', Blockly.JavaScript.ORDER_MEMBER) || 'null';
  }
  return getThis(parent);
}


export default function defineBlock({ name, init, render, precedence = null, toolboxXml = null, mutator = null, customContextMenu = null }) {
  if (!window.Blockly || !Blockly.Blocks) {
    // Blockly isn't loaded yet. Save the block for later.
    pendingBlocks.push({ name, init, render, precedence, toolboxXml, mutator, customContextMenu });
    return;
  }
  if (Blockly.Blocks[name]) {
    // The block is already defined.
    return;
  }
  Blockly.Blocks[name] = { init, customContextMenu };
  if (mutator) {
    const mutatorName = name + '_mutator';
    Blockly.Extensions.registerMutator(mutatorName, mutator, mutator.onMixin, mutator.blockList);
    Blockly.Blocks[name].init = function() {
      init.call(this);
      Blockly.Extensions.apply(mutatorName, this, true);
    };
  }
  if (precedence) {
    // It's an expression
    Blockly.JavaScript[name] = block => [
      render.call(block, block, getFieldValues(block)),
      typeof precedence === 'string' ? Blockly.JavaScript[`ORDER_${precedence}`] : precedence,
    ];
  } else {
    // It's a statement (or it takes responsibility for returning an expression)
    Blockly.JavaScript[name] = block => render.call(block, block, getFieldValues(block));
  }
  if (toolboxXml) {
    ToolboxXml[name] = toolboxXml;
  } else {
    ToolboxXml[name] = `<block type='${name}' />`;
  }
}
