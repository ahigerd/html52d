import defineBlock from './defineBlock.js';

defineBlock({
  name: 'math_point_xy',
  init() {
    this.appendDummyInput()
      .appendField('Point(')
      .appendField(new Blockly.FieldNumber(0), 'P_X')
      .appendField(',')
      .appendField(new Blockly.FieldNumber(0), 'P_Y')
      .appendField(')');
    this.setColour('#5b67a5');
    this.setOutput(true, 'Point');
  },
  render(block, { P_X, P_Y }) {
    return `new Point(${P_X}, ${P_Y})`;
  },
  precedence: 'NEW',
});
