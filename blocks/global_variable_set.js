import defineBlock from './defineBlock.js';
import { getGlobalVariables } from './objectData.js';

defineBlock({
  name: 'global_variable_set',
  init() {
    this.appendValueInput('VALUE')
      .appendField('set global')
      .appendField(new Blockly.FieldDropdown(getGlobalVariables), 'VAR')
      .appendField('to');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setInputsInline(false);
    this.setColour('#A04040');
  },
  render(block, { VAR, VALUE }) {
    return `window.${VAR} = ${VALUE('ASSIGNMENT')};\n`;
  },
  toolboxXml: () => window.editor.globalScope.variables.length ? '<block type="global_variable_set" />' : '',
  customContextMenu(menuOptions) {
    const variable = this.getFieldValue('VAR');
    if (!this.isInFlyout) {
      menuOptions.push({
        enabled: true,
        text: `Create 'get global ${variable}'`,
        callback: Blockly.ContextMenu.callbackFactory(this, Blockly.Xml.textToDom(`<xml><block type='global_variable'><field name='VAR'>${variable}</field></block></xml>`).firstChild),
      });
    }
  },
});
