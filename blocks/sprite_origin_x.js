import defineBlock, { getThis } from './defineBlock.js';

defineBlock({
  name: 'sprite_origin_x',
  init() {
    this.appendDummyInput()
      .appendField('current X position');
    this.setColour('#c09000');
    this.setOutput(true, 'number');
  },
  render(block) {
    return `${getThis(block)}.origin.x`;
  },
  precedence: 'MEMBER',
});
