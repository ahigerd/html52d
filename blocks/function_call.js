import defineBlock from './defineBlock.js';

function makeFunctionXml(fn) {
  const { name } = fn;
  let xml = `<block type='function_call'><mutation name='${name}' />`;
  for (let i = 0; i < fn.args.length; i++) {
    const arg = fn.args[i];
    if (arg.default) {
      xml += `<value name='ARG_${i}'>${arg.default.replace(/<block/g, '<shadow').replace(/<\/block/g, '</shadow')}</value>`;
    }
  }
  xml += '</block>';
  return xml;
}

defineBlock({
  name: 'function_call',
  init() {
    this.setColour('#c0c000');
  },
  mutator: {
    setFunctionName(name) {
      this.functionName = name;
      const fn = window.editor.globalScope.functions[name];
      if (!fn) {
        // still loading, try again in a tick
        window.setTimeout(() => this.setFunctionName(name), 0);
        return;
      }
      if (fn.args.length) {
        for (let i = 0; i < fn.args.length; i++) {
          const arg = fn.args[i];
          const input = this.appendValueInput(`ARG_${i}`);
          if (i === 0) {
            input.appendField(name);
          }
          input.appendField(arg.label || arg.name);
        }
      } else {
        this.appendDummyInput().appendField(name);
      }
      if (fn.returns) {
        this.setOutput(true);
      } else {
        this.setPreviousStatement(true, 'default');
        this.setNextStatement(true, 'default');
      }
    },
    domToMutation(xmlElement) {
      const name = xmlElement.getAttribute('name');
      this.setFunctionName(name);
    },
    mutationToDom() {
      const mutation = document.createElement('mutation');
      mutation.setAttribute('name', this.functionName);
      return mutation;
    },
  },
  render(block, values) {
    const fn = window.editor.globalScope.functions[block.functionName];
    const code = `${block.functionName}(${fn.args.map((_, i) => values['ARG_' + i]('COMMA')).join(', ')})`;
    if (fn.returns) {
      return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
    } else {
      return code + ';\n';
    }
  },
  toolboxXml: () => Object.values(window.editor.globalScope.functions).map(makeFunctionXml).join(''),
});
