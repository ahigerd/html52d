import defineBlock from './defineBlock.js';
import { getAllMembers } from './objectData.js';

defineBlock({
  name: 'object_member_set',
  init() {
    this.appendValueInput('THIS')
      .appendField('on object');
    this.appendDummyInput()
      .appendField('set')
      .appendField(new Blockly.FieldDropdown(getAllMembers), 'MEMBER');
    this.appendValueInput('VALUE')
      .appendField('to');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour(260);
  },
  render(block, { THIS, MEMBER, VALUE }) {
    return `${THIS('MEMBER')}.${MEMBER.split('::')[1]} = ${VALUE('ASSIGNMENT')};\n`;
  },
});


