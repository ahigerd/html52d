import defineBlock from './defineBlock.js';

defineBlock({
  name: 'object_with',
  init() {
    this.appendValueInput('THIS')
      .appendField('on object');
    this.appendStatementInput('METHODS')
      .appendField('do');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour(260);
  },
  render(block, { METHODS }) {
    // THIS is captured by getThis() inside method calls
    return METHODS();
  },
});
