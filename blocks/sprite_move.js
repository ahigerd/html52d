import defineBlock, { getThis } from './defineBlock.js';

defineBlock({
  name: 'sprite_move',
  init() {
    this.appendValueInput('P_X')
      .setCheck('Number')
      .appendField('move by');
    this.appendValueInput('P_Y')
      .setCheck('Number')
      .appendField(',');
    this.setInputsInline(true);
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour('#c09000');
  },
  render(block, { P_X, P_Y }) {
    return `${getThis(block)}.move(${P_X('COMMA')}, ${P_Y('COMMA')});\n`;
  },
  toolboxXml: `<block type='sprite_move'>
    <value name="P_X">
      <shadow type="math_number">
        <field name="NUM">0</field>
      </shadow>
    </value>
    <value name="P_Y">
      <shadow type="math_number">
        <field name="NUM">0</field>
      </shadow>
    </value>
  </block>`,
});
