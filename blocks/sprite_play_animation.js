import defineBlock, { getThis } from './defineBlock.js';
import { getCurrentPrefabAnimations } from './objectData.js';

defineBlock({
  name: 'sprite_play_animation',
  init() {
    this.appendDummyInput()
      .appendField('play animation')
      .appendField(new Blockly.FieldDropdown(getCurrentPrefabAnimations), 'ANIM')
      .appendField('once');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour('#c09000');
  },
  render(block, { ANIM }) {
    return `${getThis(block)}.playOneShot('${ANIM}');\n`;
  },
});


