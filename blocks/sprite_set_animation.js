import defineBlock, { getThis } from './defineBlock.js';
import { getCurrentPrefabAnimations } from './objectData.js';

defineBlock({
  name: 'sprite_set_animation',
  init() {
    this.appendDummyInput()
      .appendField('switch to animation')
      .appendField(new Blockly.FieldDropdown(getCurrentPrefabAnimations), 'ANIM');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour('#c09000');
  },
  render(block, { ANIM }) {
    return `${getThis(block)}.setAnimation('${ANIM}');\n`;
  },
});

