import defineBlock from './defineBlock.js';

defineBlock({
  name: 'prefab_add_to_scene',
  init() {
    this.appendValueInput('SPRITE').setCheck('Sprite')
      .appendField('add to scene');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour('#c09000');
  },
  toolboxXml: '<block type="prefab_add_to_scene"></block>',
  render(block, { SPRITE }) {
    return `scene.add(${SPRITE('NONE')});\n`;
  },
});
