import './math_point_var.js';
import './math_point_xy.js';
import { pendingBlocks } from './defineBlock.js';

pendingBlocks.push(() => {
  const math_arithmetic_init = Blockly.Blocks.math_arithmetic.init;
  Blockly.Blocks.math_arithmetic.init = function init() {
    math_arithmetic_init.call(this);
    this.setInputsInline(false);
  };
});
