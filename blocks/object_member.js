import defineBlock from './defineBlock.js';
import { getAllMembers } from './objectData.js';

defineBlock({
  name: 'object_member',
  init() {
    this.appendValueInput('THIS')
      .appendField('from object');
    this.appendDummyInput()
      .appendField('get')
      .appendField(new Blockly.FieldDropdown(getAllMembers), 'MEMBER');
    this.setOutput(true);
    this.setColour(260);
  },
  render(block, { THIS, MEMBER }) {
    return `${THIS('MEMBER')}.${MEMBER.split('::')[1]}`;
  },
  precedence: 'MEMBER',
  toolboxXml: () => window.editor.selectedFunction ? '' : '<block type="object_member" />',
});

