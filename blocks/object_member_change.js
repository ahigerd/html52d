import defineBlock from './defineBlock.js';
import { getAllMembers } from './objectData.js';

defineBlock({
  name: 'object_member_change',
  init() {
    this.appendValueInput('THIS')
      .appendField('on object');
    this.appendDummyInput()
      .appendField('change')
      .appendField(new Blockly.FieldDropdown(getAllMembers), 'MEMBER');
    this.appendValueInput('VALUE')
      .appendField('by');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour(260);
  },
  render(block, { THIS, MEMBER, VALUE }) {
    return `${THIS('MEMBER')}.${MEMBER.split('::')[1]} += ${VALUE('ASSIGNMENT')};\n`;
  },
  toolboxXml: '<sep gap="8" /><block type="object_member_change"><value name="VALUE"><shadow type="math_number"><field name="NUM">1</field></shadow></value></block>',
});


