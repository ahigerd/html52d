const commonMembers = [
  ['current position', '::origin'],
  ['current X position', '::origin.x'],
  ['current Y position', '::origin.y'],
];

export function getAllMembers() {
  const members = [...commonMembers];
  for (const prefabId of Object.keys(window.editor._assets.prefabs)) {
    const prefab = window.editor._getScope(prefabId);
    for (const variable of prefab.variables || []) {
      members.push([`(${prefabId}) ${variable}`, `${prefabId}::${variable}`]);
    }
  }
  return members;
}

export function getMembers(prefabId) {
  if (!prefabId || prefabId.endsWith('()')) {
    return [['(none)', 'undefined']];
  }
  const members = [...commonMembers];
  const prefab = window.editor._assets.prefabs[prefabId];
  for (const variable of prefab.variables) {
    members.push([variable, `${prefabId}::${variable}`]);
  }
  return members;
}

export function getCurrentMembers() {
  return getMembers(window.editor.selectedPrefab);
}

export function getGlobalVariables() {
  if (!window.editor.globalScope.variables.length) {
    return [['(none)', 'undefined']];
  }
  return window.editor.globalScope.variables.map(name => [name, name]);
}

export function getMemberLabel(member) {
  const [scope, name] = member.split('::');
  if (scope) {
    return [scope, name];
  }
  return commonMembers.find(m => m[1] == member)[0];
}

export function getCurrentPrefabAnimations() {
  const prefabId = window.editor.selectedPrefab;
  if (!prefabId) {
    return [['(none)', 'undefined']];
  }
  return Object.keys(window.editor._assets.prefabs[prefabId].config.animations).map(name => [name, name]);
}
