import defineBlock from './defineBlock.js';

defineBlock({
  name: 'function_param',
  init() {
    this.variableId = null;
    this.appendDummyInput()
      .appendField('Accept parameter named')
      .appendField(new Blockly.FieldTextInput(''), 'NAME')
      .appendField('with label')
      .appendField(new Blockly.FieldTextInput(''), 'LABEL');
    this.setPreviousStatement(true, 'fn_proto');
    this.setNextStatement(true, 'fn_proto');
    this.setColour('#c0c000');
    this.setOnChange(function (evt) {
      const newName = this.getFieldValue('NAME');
      if (this.isInFlyout) {
        this.setDisabled(false);
      } else if (this.isShadow() && !newName) {
        return;
      } else if (!this.getParent() || this.getParent().type !== 'function_entry') {
        this.setWarningText('Must be inside a function definition block.');
        if (!this.getInheritedDisabled()) {
          this.setDisabled(true);
        }
      } else if (newName) {
        this.setShadow(false);
        const existing = this.workspace.getBlocksByType('function_param');
        for (const other of existing) {
          if (other.id == this.id) {
            continue;
          }
          if (other.getFieldValue('NAME') == newName) {
            this.setWarningText(`The name '${newName}' is in use by another parameter.`);
            return;
          }
        }
        if (!this.variableId) {
          const model = this.workspace.createVariable(newName);
          this.variableId = model.getId();
        } else if (this.variableName !== newName) {
          this.variableName = newName;
          this.workspace.renameVariableById(this.variableId, newName);
        }
        this.setWarningText(null);
      } else {
        this.setWarningText('A name is required.');
        this.setDisabled(false);
      }
    });
  },
  render(block) {
    // Like event blocks, function blocks don't actually generate code of their own.
    // They provide an anchor point for code generation to start.
    return '';
  },
  toolboxXml: () => window.editor.selectedFunction ? '<block type="function_param" />' : '',
});


