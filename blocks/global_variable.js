import defineBlock from './defineBlock.js';
import { getGlobalVariables } from './objectData.js';

defineBlock({
  name: 'global_variable',
  init() {
    this.appendDummyInput()
      .appendField('global')
      .appendField(new Blockly.FieldDropdown(getGlobalVariables), 'VAR');
    this.setOutput(true);
    this.setColour('#A04040');
  },
  render(block, { VAR }) {
    return `window.${VAR}`;
  },
  precedence: 'MEMBER',
  toolboxXml: () => window.editor.globalScope.variables.length ? '<block type="global_variable" />' : '',
  customContextMenu(menuOptions) {
    const variable = this.getFieldValue('VAR');
    if (this.isInFlyout) {
      menuOptions.unshift({
        enabled: true,
        text: `Rename global variable...`,
        callback: () => window.editor.renameGlobal(variable),
      });
      menuOptions.unshift({
        enabled: true,
        text: `Delete the '${variable}' global variable`,
        callback: () => window.editor.deleteGlobal(variable),
      });
    } else {
      menuOptions.push({
        enabled: true,
        text: `Create 'set global ${variable}'`,
        callback: Blockly.ContextMenu.callbackFactory(this, Blockly.Xml.textToDom(`<xml><block type='global_variable_set'><field name='VAR'>${variable}</field></block></xml>`).firstChild),
      });
    }
  },
});

