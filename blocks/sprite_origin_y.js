import defineBlock, { getThis } from './defineBlock.js';

defineBlock({
  name: 'sprite_origin_y',
  init() {
    this.appendDummyInput()
      .appendField('current Y position');
    this.setColour('#c09000');
    this.setOutput(true, 'number');
  },
  render(block) {
    return `${getThis(block)}.origin.y`;
  },
  precedence: 'MEMBER',
});
