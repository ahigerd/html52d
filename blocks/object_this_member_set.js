import defineBlock, { getThis } from './defineBlock.js';
import { getCurrentMembers, getMemberLabel } from './objectData.js';

defineBlock({
  name: 'object_this_member_set',
  init() {
    this.appendValueInput('VALUE')
      .appendField("set this object's")
      .appendField(new Blockly.FieldDropdown(getCurrentMembers), 'MEMBER')
      .appendField('to');
    this.setPreviousStatement(true, 'default');
    this.setNextStatement(true, 'default');
    this.setColour(260);
  },
  render(block, { MEMBER, VALUE }) {
    return `this.${MEMBER.split('::')[1]} = ${VALUE('ASSIGNMENT')};\n`;
  },
  toolboxXml: () => window.editor.selectedPrefab && !window.editor.selectedFunction ? '<block type="object_this_member_set" />' : '',
  customContextMenu(menuOptions) {
    const variable = this.getFieldValue('MEMBER');
    const label = getMemberLabel(variable);
    if (!this.isInFlyout) {
      menuOptions.push({
        enabled: true,
        text: `Create 'get this object\'s ${label}'`,
        callback: Blockly.ContextMenu.callbackFactory(this, Blockly.Xml.textToDom(`<xml><block type='object_this_member'><field name='MEMBER'>${variable}</field></block></xml>`).firstChild),
      });
    }
  },
});

