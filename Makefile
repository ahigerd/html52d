help:
	@echo "Targets:"
	@echo "  make project PROJECT=/path/to/new/project"
	@echo "    - Create a new html52d project"
	@echo "  make dist"
	@echo "    - Compile html52d demos and tests"
	@echo "    - Add 'BLOCKLY=1' to include Blockly"
	@echo "  make lint"
	@echo "    - Check html52d code for style issues"
	@echo "  make clean"
	@echo "    - Remove generated temporary files"
	@echo "  make distclean"
	@echo "    - Remove all generated files"
	@echo "  make redist"
	@echo "    - Create a 'redist' folder containing deployable demos"
	@echo "  make server"
	@echo "    - Open a test server"
	@echo "    - Add 'PORT=#' to bind to port #"
	@echo ""
	@echo "See README.md for more information."

dist: dist/main.js

distclean:
	rm -rf dist redist
	rm -f build/assetlist.js
	rm -f build/assetImports.js

build/node_modules/.bin/%:
	cd build && npm install

dist/main.js: build/node_modules/.bin/webpack FORCE
	cd build && npm run build

clean: build/node_modules/.bin/webpack FORCE
	cd build && npm run clean

lint: build/node_modules/.bin/eslint
	build/node_modules/.bin/eslint -c .eslintrc.js .

redist: dist FORCE
	mkdir -p redist/assets
	mkdir -p redist/dist
	cp index.html redist
	cp assets/* redist/assets/
	cp dist/* redist/dist/

PORT=8000
server:
	python -m http.server $(PORT)

ifdef BLOCKLY
dist: blockly/blockly_compressed.js

submodules: blockly/blockly_compressed.js

blockly/.git:
	cd blockly && git submodule init

blockly/blockly_compressed.js: blockly/.git .gitmodules .git/modules/blockly/refs/heads/master
	cd blockly && git submodule update
endif

ifdef PROJECT

ifeq ($(OS),Windows_NT)

COPY = copy

$(PROJECT) $(PROJECT)/html52d:
	mkdir $(PROJECT)\\html52d

else

COPY = cp

$(PROJECT) $(PROJECT)/html52d:
	mkdir -p $(PROJECT)/html52d

endif

$(PROJECT)/.git: $(PROJECT)/html52d
	git init $(PROJECT)

project: .eslintignore .eslintrc.js .gitignore html52d/AssetStore.js html52d/Camera.js html52d/Engine.js \
		html52d/EventTarget.js html52d/KVDatabase.js html52d/KVRange.js html52d/Point.js html52d/Rect.js \
		html52d/Scene.js html52d/Sprite.js html52d/TileMap.js html52d/TouchControls.js html52d/UITools.js \
		html52d/index.js style_TouchControls.css style_UITools.css $(wildcard template/*) | $(PROJECT)/.git
	$(COPY) $(filter-out html52d/%,$^) $(PROJECT)/
	$(COPY) $(filter html52d/%,$^) $(PROJECT)/html52d/
	cd $(PROJECT) && git add $(subst template/,,$^)

else

project:
	$(error Set the project name with 'make project PROJECT=name')

endif

FORCE:
