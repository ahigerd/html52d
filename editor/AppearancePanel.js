import { buildUI } from '../html52d/UITools.js';
import { cropImage, flipImage, AnimationSequence, AnimationFrame, Hitbox } from '../html52d/Sprite.js';
import { PIXELS_PER_UNIT } from '../html52d/Point.js';
import AnimationPreview from './AnimationPreview.js';
import BoundsEditor from './BoundsEditor.js';

function cropImageAsync(frame) {
  return new Promise(resolve => {
    const [sx, sy, sw, sh] = frame.imageBounds;
    const flipX = sw < 0, flipY = sh < 0;
    const img = cropImage(frame.imageData, sx, sy, Math.abs(sw), Math.abs(sh));
    if (img.complete) {
      resolve([img, flipX, flipY]);
    } else {
      img.addEventListener('load', () => resolve([img, flipX, flipY]));
    }
  });
}

function flipImageAsync([frame, flipX, flipY]) {
  return new Promise(resolve => {
    const img = flipImage(frame, flipX, flipY);
    if (img.complete) {
      resolve(img);
    } else {
      img.addEventListener('load', () => resolve(img));
    }
  });
}

export default class AppearancePanel {
  constructor(container) {
    this.container = container;
    this._ui = buildUI(this, container, null, { AnimationPreview, BoundsEditor });

    this._currentAnim = null;
  }

  get _animation() {
    if (!this._prefab || !this._currentAnim) {
      return null;
    }

    return this._prefab.config.animations[this._currentAnim] || null;
  }

  set _animation(val) {
    this._prefab.config.animations[this._currentAnim] = val;
  }


  show() {
    this.container.style.display = '';
    this._ui.animPreview.start();
  }

  hide() {
    this.container.style.display = 'none';
    this._ui.animPreview.stop();
  }

  load(prefab) {
    if (!prefab) {
      this._ui.animList.clear();
      this._ui.animPreview.clear();
      return;
    }
    this._prefab = prefab;
    const { config } = prefab;
    const animations = config.animations && Object.keys(config.animations).length > 0 ? config.animations : { default: [] };
    const defaultAnim = animations[config.defaultAnimationName] ? config.defaultAnimationName : 'default';
    console.log(config);
    this._ui.animName.setOptions(Object.keys(animations).map(value => value === defaultAnim ? { value, label: `${value} (default)` } : value));
    this._ui.animName.value = defaultAnim;
    this._ui.animateHitboxes.checked = config.animateHitboxes;
    this._selectAnimation(defaultAnim);
  }

  _selectAnimation(nameOrEvent) {
    const name = typeof nameOrEvent === 'string' ? nameOrEvent : event.target.value;
    this._currentAnim = name;
    let anim = normalizeSequenceClass(this._animation);
    if (!anim) {
      this._ui.animList.clear();
      this._ui.animPreview.clear();
      return;
    }
    this._selectFrame(-1);
    this._ui.animMs.value = anim.frameMS;
    Promise.all(anim.frames.map(cropImageAsync))
      .then(images => Promise.all(images.map(flipImageAsync)))
      .then(images => this._ui.animList.setOptions(images.map((label, value) => ({ label, value }))))
      .then(() => {
        this._saveAnimation();
        this._ui.animPreview.start(this._animation);
      });
  }
  _setAnimMs(event) {
    const anim = this._animation;
    if (!anim) {
      return;
    }

    const ms = parseInt(this._ui.animMs.value) || 16;

    if (anim instanceof AnimationSequence) {
      anim.frameMS = ms;
      anim.framesPerMs = 1.0 / ms;
    } else {
      anim[anim.length - 1] = ms;
    }
  }

  _verifyAnimMs(event) {
    const anim = this._animation;
    if (anim) {
      this._ui.animMs.value = anim instanceof AnimationSequence ? anim.frameMS : anim[anim.length - 1];
    }
  }

  _selectFrame(eventOrFrame) {
    const frame = (eventOrFrame.target ? eventOrFrame.target.value : eventOrFrame) || 0;
    if (frame < 0 || frame >= this._ui.animList.options.length) {
      this._ui.animList.value = undefined;
      this._ui.moveUp.disabled = true;
      this._ui.moveDown.disabled = true;
      this._ui.remove.disabled = true;
      this._ui.clone.disabled = true;
      return;
    }
    this._ui.animList.value = frame;
    this._ui.moveUp.disabled = (frame === 0);
    this._ui.moveDown.disabled = (frame === this._ui.animList.options.length - 1);
    this._ui.remove.disabled = false;
    this._ui.clone.disabled = false;

    const anim = this._animation;
    const frameObj = anim.frames[frame];
    this._ui.hitbox.setContent({
      image: frameObj.imageData,
      origin: frameObj.origin,
      bounds: this._prefab.config.animateHitboxes ? frameObj.hitboxes[0] : this._prefab.config.hitbox,
      world: true,
    });
  }

  _moveFrameUp() {
    const frame = this._ui.animList.value;
    const opts = this._ui.animList.options;
    const t = opts[frame].label.src;
    opts[frame].label.src = opts[frame - 1].label.src;
    opts[frame - 1].label.src = t;
    this._selectFrame(frame - 1);
    this._saveAnimation();
  }

  _moveFrameDown() {
    const frame = this._ui.animList.value;
    const opts = this._ui.animList.options;
    const t = opts[frame].label.src;
    opts[frame].label.src = opts[frame + 1].label.src;
    opts[frame + 1].label.src = t;
    this._selectFrame(frame + 1);
    this._saveAnimation();
  }

  _cloneFrame() {
    const frame = this._ui.animList.value;
    const img = document.createElement('IMG');
    img.src = this._ui.animList.options[frame].label.src;
    this._ui.animList.addOption(img, this._ui.animList.options.length);
    this._selectFrame(this._ui.animList.options.length - 1);
    this._saveAnimation();
  }

  _addFrame() {
    this._ui.addFrameDialog.uploadFile.value = null;
    this._ui.addFrameDialog.show();
  }

  _dragOver(event) {
    event.stopPropagation();
    event.preventDefault();
    event.dataTransfer.dropEffect = 'copy';
  }

  _dropFile(event) {
    event.stopPropagation();
    if (event.type == 'drop') {
      event.preventDefault();
    }
    const files = (event.dataTransfer || event.target).files;
    const file = files[0];

    const img = document.createElement('IMG');
    this._newFrameRevertImage = img;
    const reader = new FileReader();
    reader.onload = e => img.src = e.target.result;
    img.onload = e => {
      this._newFrameRevert();
      this._ui.cropFrameDialog.show();
    }
    reader.readAsDataURL(file);

    this._ui.addFrameDialog.hide();
  }

  _dragExit(event) {
    event.stopPropagation();
    event.preventDefault();
  }

  _updateNewFrameImage(img) {
    this._newFrameImage = img;
    this._ui.cropFrameDialog.frameEditor.setContent({
      image: img,
      bounds: [0, 0, img.width, img.height],
      world: false,
    });
  }

  _newFrameRevert() {
    this._updateNewFrameImage(this._newFrameRevertImage);
  }

  _newFrameCrop() {
    const bounds = this._ui.cropFrameDialog.frameEditor.bounds;
    const cropped = cropImage(this._newFrameImage, bounds.x1 | 0, bounds.y1 | 0, bounds.width | 0, bounds.height | 0);
    cropped.onload = () => this._updateNewFrameImage(cropped);
  }

  _newFrameFlipX() {
    const flipped = flipImage(this._newFrameImage, true, false);
    flipped.onload = () => this._updateNewFrameImage(flipped);
  }

  _newFrameFlipY() {
    const flipped = flipImage(this._newFrameImage, false, true);
    flipped.onload = () => this._updateNewFrameImage(flipped);
  }

  _newFrameDone() {
    this._ui.animList.addOption(this._newFrameImage, this._ui.animList.options.length);
    this._saveAnimation();
    setTimeout(() => this._selectFrame(this._ui.animList.options.length - 1), 0);
    this._ui.cropFrameDialog.hide();
  }

  _newFrameCancel() {
    this._ui.cropFrameDialog.hide();
  }

  _removeFrame() {
    const options = this._ui.animList.options;
    const frame = this._ui.animList.value;
    this._ui.animList.clear();
    let i = 0;
    for (const opt of options) {
      if (opt.value !== frame) {
        this._ui.animList.addOption(opt.label, i);
        i++;
      }
    }
    this._selectFrame(frame >= this._ui.animList.options.length ? this._ui.animList.options.length - 1 : frame);
    this._saveAnimation();
  }

  _saveAnimation() {
    const frames = this._ui.animList.options.map(frame => new AnimationFrame(frame.label));
    const ms = parseInt(this._ui.animMs.value) || 16;
    const anim = this._animation;
    if (anim instanceof AnimationSequence) {
      // Reuse the existing object if possible in order to allow running instances in the scene to dynamically update
      anim.frames = frames;
      anim.framesMS = ms;
      anim.framesPerMs = 1 / ms;
    } else {
      this._animation = new AnimationSequence(frames, ms);
    }
  }

  _setAnimateHitboxes() {
    if (this._ui.animateHitboxes.checked) {
      this._prefab.config.animateHitboxes = true;
      for (const frame of this._animation.frames) {
        frame.hitboxes = [new Hitbox(this._prefab.config.hitbox)];
      }
    } else {
      this._prefab.config.animateHitboxes = false;
    }
  }

  _updateHitbox() {
    const frameObj = this._animation.frames[this._ui.animList.value];
    if (this._prefab.config.animateHitboxes) {
      frameObj.hitboxes = [this._ui.hitbox.bounds];
    } else {
      this._prefab.config.hitbox = this._ui.hitbox.bounds;
      for (const frame of this._animation.frames) {
        frame.hitboxes = [this._ui.hitbox.bounds];
      }
    }
    frameObj.origin = this._ui.hitbox.origin;
  }
};
