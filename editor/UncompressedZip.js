const fileHeaderSize = 30;

const fileHeader = Uint8Array.from([
  0x50, 0x4b, 0x03, 0x04, // magic number
  0x02, 0x00, // requires folder support
  0x00, 0x08, // bit 11: UTF-8 filenames 
  0x00, 0x00, // stored uncompressed
]);

const centralHeader = Uint8Array.from([
  0x50, 0x4b, 0x01, 0x02, // magic number
  0x00, 0x00, // compatible with 2.04g
  0x02, 0x00, // requires folder support
  0x00, 0x08, // bit 11: UTF-8 filenames 
  0x00, 0x00, // stored uncompressed
]);

const endHeader = Uint8Array.from([
  0x50, 0x4b, 0x05, 0x06, // magic number
  0x00, 0x00, // current disk number
  0x00, 0x00, // directory disk number
]);

function dateToTimestamp(date) {
  const timeBits = 
    (Math.floor(date.getSeconds() / 2)) |
    (date.getMinutes() << 5) |
    (date.getHours() << 11);
  const dateBits =
    date.getDate() |
    ((date.getMonth() + 1) << 5) |
    ((date.getFullYear() - 1980) << 9);
  return Uint8Array.from([
    timeBits & 0x00FF,
    (timeBits & 0xFF00) >>> 8,
    dateBits & 0x00FF,
    (dateBits & 0xFF00) >>> 8,
  ]);
}

let dataToBlob;
if (window.BlobBuilder || window.MSBlobBuilder) {
  // Old deprecated API but it's all Edge supports
  dataToBlob = function dataToBlob(data, type = 'application/octet-stream') {
    const builder = new (window.BlobBuilder || window.MSBlobBuilder)();
    for (const segment of data) {
      if (Array.isArray(segment)) {
        builder.append(Uint8Array.from(segment));
      } else {
        builder.append(segment);
      }
    }
    return builder.getBlob('application/octet-stream');
  }
} else {
  // New standard API but not all browsers support it
  dataToBlob = function dataToBlob(data, type = 'application/octet-stream') {
    const segments = data.map(segment => Array.isArray(segment) ? Uint8Array.from(segment) : segment);
    return new Blob(segments, { type });
  }
}

function dataToBytes(data) {
  return new Promise(resolve => {
    const blob = dataToBlob(data);
    const reader = new FileReader();
    reader.onload = event => resolve(new Uint8Array(event.target.result));
    reader.readAsArrayBuffer(blob);
  });
}

function uint32To8(value) {
  return [
    value & 0xFF,
    (value >>> 8) & 0xFF,
    (value >>> 16) & 0xFF,
    (value >>> 24) & 0xFF,
  ];
}

function uint16To8(value) {
  return [
    value & 0xFF,
    (value >>> 8) & 0xFF,
  ];
}

function crc32(data) {
  let crc = 0xFFFFFFFF;
  for (const ch of data) {
    crc ^= ch;
    for (let j = 7; j >= 0; --j) {
      const mask = -(crc & 1);
      crc = (crc >>> 1) ^ (0xEDB88320 & mask);
    }
  }
  return uint32To8(~crc);
}

export default class UncompressedZip {
  constructor() {
    this.files = {};
  }

  addFile(filename, content) {
    this.files[filename] = (Array.isArray(content) ? content : [content]);
  }

  toBlob() {
    return Promise.all(Object.keys(this.files).map(filename => this._fileData(filename)))
      .then(chunks => {
        const data = [];
        const central = [];
        let offset = 0;
        for (const chunk of chunks) {
          data.push(...chunk.file);
          chunk.central[10] = uint32To8(offset);
          offset += chunk.offsetSize;
        }
        let centralSize = 0;
        for (const chunk of chunks) {
          centralSize += chunk.central.reduce((sum, x) => sum + x.length, 0)
          data.push(...chunk.central);
        }
        data.push(
          endHeader,
          uint16To8(chunks.length),
          uint16To8(chunks.length),
          uint32To8(centralSize),
          uint32To8(offset),
          [0, 0], // comment length
        );
        return dataToBlob(data, 'application/zip', 'test.zip');
      });
  }

  _fileData(filename) {
    const file = this.files[filename];
    return dataToBytes(file)
      .then(bytes => dataToBytes([filename]).then(fnBytes => ({ bytes, fnBytes }))
      .then(({ bytes, fnBytes }) => {
        const timestamp = dateToTimestamp(new Date());
        const crc = crc32(bytes);
        const size = uint32To8(bytes.length);
        const fnLength = uint16To8(fnBytes.length);

        return {
          offsetSize: fileHeaderSize + fnBytes.length + bytes.length,
          file: [
            fileHeader,
            timestamp,
            crc,
            size,
            size,
            fnLength,
            [0, 0], // extra field size
            fnBytes,
            bytes, 
          ],
          central: [
            centralHeader,
            timestamp,
            crc,
            size,
            size,
            fnLength,
            [0, 0], // extra field size
            [0, 0], // comment size
            [0, 0], // disk number
            [0, 0, 0, 0, 0, 0], // file attributes
            'XXX: placeholder', // relative file offset
            fnBytes,
          ],
        };
    }));
  }
};
