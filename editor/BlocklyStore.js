import KVDatabase from '../html52d/KVDatabase.js';
import KVRange from '../html52d/KVRange.js';
import FunctionDef from './FunctionDef.js';

const dbMigrations = [
  txn => {
    const functions = txn.createStore('functions', 'key');
    functions.createIndex('name', 'name', { unique: false });
    functions.createIndex('scope', 'scope', { unique: false });
  },
  txn => {
    const vars = txn.createStore('variables', 'scope');
  },
];

export default class BlocklyStore {
  constructor(name = 'Html52dBlocklyStore') {
    this.db = new KVDatabase(name, dbMigrations);
  }

  static deleteDatabase(name = 'Html52dBlocklyStore') {
    return KVDatabase.deleteDatabase(name);
  }

  getEventsForScene() {
    return this.getEventsForPrefab('');
  }

  getEventsForPrefab(name) {
    return this.db.readTransaction('functions', txn => txn.functions.index('scope').get(KVRange.equals(name)));
  }

  getPrefabNames() {
    return this.db.readTransaction('functions', txn => txn.functions.index('scope').getKeys())
      .then(keys => keys.filter(k => k && k[0] != '$'));
  }

  getVariables(scope) {
    return this.db.readTransaction('variables', txn => txn.variables.index('scope').get(KVRange.equals(scope)))
      .then(rows => rows[0].variables, () => []);
  }

  getAllVariables() {
    return this.db.readTransaction('variables', txn => txn.variables.get())
      .then(rows => {
        const result = {};
        for (const row of rows) {
          result[row.scope] = row.variables;
        }
        return result;
      });
  }

  saveVariables(scope, variables) {
    return this.db.writeTransaction('variables', txn => txn.variables.put({ scope, variables }));
  }

  getFunctions(scope = undefined) {
    return this.db.readTransaction('functions', txn => txn.functions.get(scope === undefined ? undefined : KVRange.equals(scope)))
      .then(rows => rows.map(row => FunctionDef.fromStore(row)));
  }

  saveFunction(fn) {
    const { name, blockXml } = fn;
    const scope = fn.className === null ? '$global' : fn.className;
    const record = {
      key: `${scope}::${name}`,
      scope,
      name,
      blockXml,
    };
    if (fn.className === null) {
      record.args = fn.args.map(arg => arg.name);
      record.returns = fn.returns;
    }
    return this.db.writeTransaction('functions', txn => txn.functions.put(record));
  }
};
