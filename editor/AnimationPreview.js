export default class AnimationPreview {
  constructor(element) {
    this._img = element;
    this.animation = null;
    this._previewStartTime = 0;
    this._updatePreview = this._updatePreview.bind(this);
  }

  start(anim) {
    if (anim) {
      this.animation = anim;
    }
    this.stop();
    this._previewStartTime = Date.now();
    this._updatePreview();
  }

  stop() {
    if (this._previewTimer) {
      cancelAnimationFrame(this._previewTimer);
      this._previewTimer = null;
    }
  }

  clear() {
    this.stop();
    this._img.src = '';
  }

  _updatePreview() {
    if (!this.animation || !this.animation.length || !this.animation.frameAt) {
      this.clear();
      return;
    }
    const pos = (Date.now() - this._previewStartTime) % this.animation.duration;
    const frame = this.animation.frameAt(pos);
    this._img.src = frame.imageData.src;
    this._img.style.left = `${this._img.parentElement.offsetWidth / 2 - (frame.width - frame.origin.x) * PIXELS_PER_UNIT}px`;
    this._img.style.top = `${this._img.parentElement.offsetHeight / 2 - frame.origin.y * PIXELS_PER_UNIT}px`;
    this._previewTimer = requestAnimationFrame(this._updatePreview);
  }
};
