import { Rect } from '../html52d/Rect.js';
import { Point, PIXELS_PER_UNIT } from '../html52d/Point.js';
import { UIElement } from '../html52d/UITools.js';

class Handle {
  constructor(cursor, color, obj, xKey, yKey) {
    this.cursor = cursor;
    this.color = color;
    this.obj = obj;
    this.xKey = xKey;
    this.yKey = yKey;
  }

  get x() {
    return this.obj[this.xKey];
  }

  set x(val) {
    this.obj[this.xKey] = val;
  }

  get y() {
    return this.obj[this.yKey];
  }

  set y(val) {
    this.obj[this.yKey] = val;
  }

  contains(x, y) {
    return Math.abs(x - this.x) <= 4 && Math.abs(y - this.y) <= 4;
  }

  render(ctx) {
    ctx.strokeStyle = this.color;
    ctx.strokeRect(this.x - 3.5, this.y - 3.5, 6, 6);
  }
};

export default class BoundsEditor extends UIElement {
  constructor(element, config = null) {
    super(element, 'CANVAS');
    if (!config) {
      config = element.dataset || {};
    }
    element.classList.add('bounds-editor');
    this._ctx = element.getContext('2d');
    this._width = element.width;
    this._height = element.height;
    this._ctx.webkitImageSmoothingEnabled = false;
    this._ctx.mozImageSmoothingEnabled = false;
    this._ctx.imageSmoothingEnabled = false;

    element.addEventListener('mousedown', this._onMouseDown.bind(this));
    element.addEventListener('mousemove', this._onMouseMove.bind(this));
    element.addEventListener('mouseup', this._onMouseUp.bind(this));

    if (typeof config.origin === 'string') {
      this._showOrigin = config.origin.toLowerCase() === 'true';
    } else {
      this._showOrigin = !!config.origin;
    }

    this._origin = new Point(0, 0);
    this._bounds = new Rect(0, 0, 0, 0);
    this.setContent({
      origin: new Point(this._width / 2, this._height / 2),
      bounds: new Rect(this._width / 4, this._height / 4, this._width * .75, this._height * .75),
      image: null,
    });

    this._handles = [
      new Handle('nwse-resize', 'black', this._bounds, 'x1', 'y1'),
      new Handle('ew-resize', 'black', this._bounds, 'x1', 'ym'),
      new Handle('nesw-resize', 'black', this._bounds, 'x1', 'y2'),
      new Handle('ns-resize', 'black', this._bounds, 'xm', 'y1'),
      new Handle('ns-resize', 'black', this._bounds, 'xm', 'y2'),
      new Handle('nesw-resize', 'black', this._bounds, 'x2', 'y1'),
      new Handle('ew-resize', 'black', this._bounds, 'x2', 'ym'),
      new Handle('nwse-resize', 'black', this._bounds, 'x2', 'y2'),
    ];
    if (this._showOrigin) {
      this._handles.unshift(new Handle('move', 'red', this._origin, 'x', 'y'));
    }
    this._selectedHandle = null;

    this._redraw();
  }

  setContent({ image, bounds, origin, world }) {
    this._img = image;
    if (!image) {
      this._scale = 1;
      return;
    }
    this._world = world;
    this._scale = Math.max(1, Math.floor(Math.min(this._width / image.width, this._height / image.height) / 2));
    const w = image.width * this._scale;
    const h = image.height * this._scale;
    const x = ((this._width - w) / 2) | 0;
    let y = ((this._height - h) / 2 + (world ? 0 : h)) | 0;
    const scaleFactor = this._scale * (world ? PIXELS_PER_UNIT : 1);
    if (this._showOrigin) {
      this._origin.setXY(
        scaleFactor * origin[0] + x,
        scaleFactor * origin[1] + y,
      );
    } else {
      this._origin.setXY(x, y);
    }
    this._bounds.setCoords(
      scaleFactor * bounds[0] + this._origin[0],
      this._origin[1] - scaleFactor * bounds[3],
      scaleFactor * bounds[2] + this._origin[0],
      this._origin[1] - scaleFactor * bounds[1],
    );
    this._bounds.xm = this._bounds.x1 + (this._bounds.width / 2) | 0;
    this._bounds.ym = this._bounds.y1 + (this._bounds.height / 2) | 0;
    this._redraw();
  }

  get origin() {
    const scaleFactor = this._scale * (this._world ? PIXELS_PER_UNIT : 1);
    const w = this._img.width * this._scale;
    const h = this._img.height * this._scale;
    const x = ((this._width - w) / 2) | 0;
    const y = ((this._height - h) / 2) | 0;
    return new Point((this._origin[0] - x) / scaleFactor, (this._origin[1] - y) / scaleFactor);
  }

  get bounds() {
    if (this._world) {
      const scaleFactor = this._scale * PIXELS_PER_UNIT;
      return new Rect(
        (this._bounds[0] - this._origin[0]) / scaleFactor,
        (this._origin[1] - this._bounds[1]) / scaleFactor,
        (this._bounds[2] - this._origin[0]) / scaleFactor,
        (this._origin[1] - this._bounds[3]) / scaleFactor,
      );
    } else {
      return new Rect(
        (this._bounds[0] - this._origin[0]) / this._scale,
        this._img.height - (this._origin[1] - this._bounds[1]) / this._scale,
        (this._bounds[2] - this._origin[0]) / this._scale,
        this._img.height - (this._origin[1] - this._bounds[3]) / this._scale,
      );
    }
  }

  _onMouseMove({ offsetX, offsetY, buttons, shiftKey }) {
    let cursor = '';
    if (buttons && this._selectedHandle) {
      const beforeX = this._selectedHandle.x;
      const beforeY = this._selectedHandle.y;
      cursor = 'crosshair';
      if (shiftKey && (cursor === 'nesw-resize' || cursor === 'nwse-resize')) {
        const dx = offsetX - this._selectedHandle.x;
        const dy = offsetY - this._selectedHandle.y;
        const d = Math.min(Math.abs(dx), Math.abs(dy));
        if (cursor === 'nwse-resize') {
          if (Math.abs(dx) === d) {
            this._selectedHandle.x += dx;
            this._selectedHandle.y += dx;
          } else {
            this._selectedHandle.x += dy;
            this._selectedHandle.y += dy;
          }
        } else {
          if (Math.abs(dx) === d) {
            this._selectedHandle.x += dx;
            this._selectedHandle.y -= dx;
          } else {
            this._selectedHandle.x -= dy;
            this._selectedHandle.y += dy;
          }
        }
      } else {
        this._selectedHandle.x = offsetX;
        this._selectedHandle.y = offsetY;
      }
      this._bounds.xm = this._bounds.x1 + (this._bounds.width / 2) | 0;
      this._bounds.ym = this._bounds.y1 + (this._bounds.height / 2) | 0;
      this._redraw();
      if (beforeX !== this._selectedHandle.x || beforeY !== this._selectedHandle.y) {
        this.dispatchEvent('change');
      }
    } else {
      for (const h of this._handles) {
        if (h.contains(offsetX, offsetY)) {
          cursor = h.cursor;
          break;
        }
      }
    }
    this._element.style.cursor = cursor;
  }

  _onMouseDown({ offsetX, offsetY, button, buttons, shiftKey }) {
    for (const h of this._handles) {
      if (h.contains(offsetX, offsetY)) {
        this._selectedHandle = h;
        break;
      }
    }
    this._onMouseMove({ offsetX, offsetY, buttons, shiftKey });
  }

  _onMouseUp({ offsetX, offsetY, button, buttons }) {
    this._selectedHandle = null;
    this._onMouseMove({ offsetX, offsetY, buttons });
  }

  _redraw() {
    this._ctx.clearRect(0, 0, this._width, this._height);
    if (!this._img) {
      return;
    }

    const w = this._img.width * this._scale;
    const h = this._img.height * this._scale;
    const x = ((this._width - w) / 2) | 0;
    const y = ((this._height - h) / 2) | 0;
    this._ctx.drawImage(this._img, x, y, w, h);

    if (this._showOrigin) {
      this._ctx.fillStyle = '#c0c0c0';
      this._ctx.fillRect(0, this._origin.y - 1, this._width, .5);
      this._ctx.fillRect(this._origin.x - 1, 0, .5, this._height);
    }
    this._ctx.strokeStyle = 'black';
    this._ctx.strokeRect(this._bounds.x1 - .5, this._bounds.y1 - .5, this._bounds.width, this._bounds.height);
    for (let i = this._handles.length - 1; i >= 0; --i) {
      this._handles[i].render(this._ctx);
    }
  }
};
