import { ToolboxXml as _ToolboxXml, addPendingBlocks } from '../blocks/defineBlock.js';
/* global Blockly:false */

export const ToolboxXml = _ToolboxXml;

function loadScript(fn) {
  return new Promise(resolve => {
    const tag = document.createElement('SCRIPT');
    tag.addEventListener('load', resolve);
    tag.src = `blockly/${fn}`;
    document.body.appendChild(tag);
  });
}

export function loadBlockly(generator = 'JavaScript', language = 'en') {
  const alreadyLoaded = window.Blockly;
  return Promise.resolve(alreadyLoaded || loadScript('blockly_compressed.js'))
    .then(() => window.Blockly.Msg[language] || loadScript(`msg/js/${language}.js`))
    .then(() => Promise.all([
      alreadyLoaded || loadScript('blocks_compressed.js'),
      (generator && window.Blockly[generator]) || loadScript(`${generator.toLowerCase()}_compressed.js`),
    ]))
    .then(addPendingBlocks);
}

export function injectBlockly(div, config) {
  let promise = loadBlockly();
  let toolbox;
  if (config.toolboxPath) {
    promise = promise.then(() => fetch(config.toolboxPath))
      .then(response => response.text())
      .then(xml => toolbox = xml);
  }
  return promise.then(() => Blockly.inject(div, {
    toolbox,
    ...config,
  })).then(workspace => {
    window.blocklyWorkspace = workspace;
    return workspace;
  });
}
