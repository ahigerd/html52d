import EventTarget from '../html52d/EventTarget.js';
import { loadBlockly, ToolboxXml } from './BlocklyTools.js';
/* global Blockly:false */
window.ToolboxXml = ToolboxXml;

const FUNC_NAME = 0;
const FUNC_ARGS = 1;
const FUNC_RETURN = 2;

export default class BlocklyWorkspace extends EventTarget {
  constructor(config) {
    super();
    this._loadPromise = Promise.all([
      config.toolboxPath ? fetch(config.toolboxPath).then(response => response.text()) : Promise.resolve(config.toolbox),
      loadBlockly(config.generator || 'JavaScript', config.language || 'en'),
    ]);
    this._getWorkspace = new Promise(resolve => this._injectResolve = resolve);
    this._config = {
      generator: 'JavaScript',
      language: 'en',
      ...config,
    };
    this._container = null;
    this._pendingToolboxRefresh = 0;
    this._syncWorkspace = null;
    this._generator = null;
    this._withWorkspace(workspace => {
      this._syncWorkspace = workspace;
      for (const block of Object.values(Blockly.Blocks)) {
        const realInit = block.init;
        block.init = function() {
          realInit.call(this);
          if (this.previousConnection) {
            const prevCheck = this.previousConnection.getCheck();
            if (!prevCheck) {
              this.previousConnection.setCheck(['default']);
            }
          }
          if (this.nextConnection) {
            const nextCheck = this.nextConnection.getCheck();
            if (!nextCheck) {
              this.nextConnection.setCheck(['default']);
            }
          }
        };
      }
    });
    this.toolboxCategories = {};
    this._buttonCallbacks = {};
    this._nextButtonId = 1;
    this._buttonDisabled = {};
  }

  get style() {
    return this._container && this._container.style;
  }

  _withWorkspaceSync(cb) {
    if (!this._syncWorkspace || !Blockly || !Blockly[this._config.generator]) {
      throw new TypeError('workspace not ready');
    }
    return cb(this._syncWorkspace);
  }

  _initGenerator(cb) {
    return this._withWorkspaceSync(workspace => {
      if (this._generator && !this._generator.variableDB_) {
        this._generator.init(workspace);
      }
      return cb(workspace);
    });
  }

  _withWorkspace(cb) {
    return this._getWorkspace = this._getWorkspace.then(ws => { cb(ws); return ws; });
  }

  inject(div) {
    if (this._container) {
      throw new TypeError('cannot inject Blockly workspace that has already been injected');
    }
    this._container = div;
    return this._loadPromise.then(([toolbox]) => {
      this.toolboxXml = toolbox;
      const workspace = Blockly.inject(div, { ...this._config, toolbox });
      this._generator = this._config.generator && Blockly[this._config.generator];
      workspace.addChangeListener(() => this._withWorkspace(ws => this.dispatchEvent('workspacechange', ws)));
      this.dispatchEvent(new UIEvent('load'));
      this._injectResolve(workspace);
    });
  }

  _getToolboxCategory(name) {
    if (!this.toolboxCategories[name]) {
      this.toolboxCategories[name] = [];
    }
    return this.toolboxCategories[name];
  }

  _getToolboxCategoryCallback(name) {
    const category = this._getToolboxCategory(name);
    return () => category
      .map(name => ToolboxXml[name])
      .map(xml => typeof xml === 'function' ? xml(this) : xml)
      .map(xml => xml ? [...Blockly.Xml.textToDom(`<xml>${xml}</xml>`).childNodes] : [])
      .reduce((a, b) => a.concat(b));
  }

  refreshToolbox() {
    this._pendingToolboxRefresh++;
    this._withWorkspace(workspace => {
      this._pendingToolboxRefresh--;
      if (!this._pendingToolboxRefresh) {
        workspace.getToolbox().refreshSelection();
      }
    });
  }

  updateToolbox(xml) {
    if (typeof xml === 'string') {
      xml = Blockly.Xml.textToDom(`<xml>${xml}</xml>`);
    }
    return this._withWorkspace(workspace => {
      workspace.updateToolbox(xml);
      this.toolboxXml = xml;
    });
  }

  registerToolboxCategory(name) {
    this._withWorkspace(workspace => {
      workspace.registerToolboxCategoryCallback(name, this._getToolboxCategoryCallback(name));
      this.refreshToolbox();
    });
  }

  addButtonToToolbox(categoryName, label, callback) {
    const category = this._getToolboxCategory(categoryName);
    const id = `button_${this._nextButtonId}`;
    ++this._nextButtonId;
    category.push(id);
    ToolboxXml[id] = () => this._buttonDisabled[id] ? '' : `<button text="${label.replace(/"/g, '&quot;')}" callbackKey="${id}" />`;
    this._withWorkspace(workspace => {
      workspace.registerButtonCallback(id, callback);
    });
    return id;
  }

  setButtonEnabled(id, enable) {
    this._buttonDisabled[id] = !enable;
  }

  addBlockToToolbox(categoryName, name) {
    const category = this._getToolboxCategory(categoryName);
    if (!category.includes(name)) {
      category.push(name);
      this.refreshToolbox();
    }
  }

  removeBlockFromToolbox(categoryName, name) {
    const category = this._getToolboxCategory(categoryName);
    const index = category.indexOf(name);
    if (index >= 0) {
      category.splice(index, 1);
      this.refreshToolbox();
    }
  }

  clearToolboxCategory(categoryName) {
    this.toolboxCategories[categoryName] = [];
    this.refreshToolbox();
  }

  loadXml(xml) {
    this._withWorkspaceSync(workspace => Blockly.Xml.clearWorkspaceAndLoadFromXml(xml, workspace));
  }

  getDom() {
    return this._withWorkspaceSync(workspace => Blockly.Xml.workspaceToDom(workspace));
  }

  getXml() {
    return this.getDom().outerHTML;
  }

  getAllCode() {
    return this._withWorkspaceSync(workspace => this._generator.workspaceToCode(workspace));
  }

  getBlockById(id) {
    return this._withWorkspaceSync(workspace => workspace.getBlockById(id));
  }

  getBlocksByType(type) {
    return this._withWorkspaceSync(workspace => workspace.getBlocksByType(type));
  }

  getBlocksByTypes(types) {
    return [].concat(...types.map(type => this.getBlocksByType(type)));
  }

  getCodeForBlock(blockOrId) {
    const block = typeof blockOrId === 'string' ? this.getBlockById(blockOrId) : blockOrId;
    return this._initGenerator(() => this._generator.blockToCode(block));
  }

  getFunctions() {
    return this._initGenerator(workspace => {
      const result = {};
      const db = this._generator ? this._generator.variableDB_ : { getName: x => x };
      for (const fn of [].concat(...Blockly.Procedures.allProcedures(workspace))) {
        const block = Blockly.Procedures.getDefinition(fn[FUNC_NAME], workspace);
        const retInput = block.getInput('RETURN');
        const returnBlock = retInput && retInput.connection && retInput.connection.targetBlock && retInput.connection.targetBlock();
        const funcData = {
          rawName: fn[FUNC_NAME],
          name: db.getName(fn[FUNC_NAME], 'PROCEDURE'),
          args: fn[FUNC_ARGS].map(arg => db.getName(arg, 'VARIABLE')),
          hasReturn: fn[FUNC_RETURN],
          block,
          returnBlock,
          body: block.getChildren().filter(c => c !== returnBlock)[0],
          getCode: () => this._getCodeForFunction(funcData),
        };
        result[fn[FUNC_NAME]] = funcData;
      }
      return result;
    });
  }

  getVariables() {
    return this._withWorkspaceSync(workspace => {
      const vmap = workspace.getVariableMap();
      const pmap = workspace.getPotentialVariableMap();
      const vars = [
        ...(vmap ? vmap.getAllVariables() : []),
        ...(pmap ? pmap.getAllVariables() : []),
      ];
      const result = {};
      for (const v of vars) {
        const id = v.getId();
        if (!result[id]) {
          result[id] = {
            name: v.name,
            type: v.type,
            id,
          };
        }
      }
      return result;
    });
  }

  ensureVariables(vars) {
    return this._withWorkspace(workspace => {
      for (const id of Object.keys(vars)) {
        if (!workspace.getVariableById(id)) {
          workspace.createVariable(vars[id].name, vars[id].type, id);
        }
      }
    });
  }

  _getCodeForFunction(funcData) {
    // Some functions don't generate code directly but instead store their definitions in the generator.
    // These are by convention decorated with a prefixed %, but this is not required; third-party generators
    // may require additional code here.
    return this.getCodeForBlock(funcData.block) ||
      this._generator.definitions_['%' + funcData.name] ||
      this._generator.definitions_[funcData.name];
  }
}
