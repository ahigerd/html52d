import eventTypes from '../blocks/eventTypes.js';

export default class FunctionDef {
  constructor(name, className = null) {
    this.name = name;
    this.className = className === '$global' ? null : className;
    this._blockXml = '';
    this.args = null;
    this.returns = null;
  }

  static fromStore(record) {
    const def = new FunctionDef(record.name, record.scope);
    // Bypass proper initialization since there might be recursion
    def._blockXml = record.blockXml;
    if (def.className === null) {
      def.args = record.args;
      def.returns = record.returns;
    } else if (eventTypes[record.name]) {
      def.args = eventTypes[record.name].args;
      def.returns = eventTypes[record.name].returns;
    }
    return def;
  }

  get blockXml() {
    return this._blockXml;
  }

  set blockXml(xml) {
    this._blockXml = xml;
    this._inNewWorkspace(ws => {
      const startBlock = ws.getBlocksByType('function_entry')[0];
      this.args = startBlock.getArguments();
      this.returns = startBlock.getReturns();
    });
  }

  get isMember() {
    return this.className !== null;
  }

  _inNewWorkspace(fn) {
    const ws = new Blockly.Workspace();
    try {
      Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom(this.blockXml), ws);
      return fn(ws);
    } finally {
      ws.dispose();
    }
  }

  renameGlobal(before, after) {
    return this._inNewWorkspace(ws => {
      for (const type of ['global_variable', 'global_variable_set', 'global_variable_change']) {
        for (const block of ws.getBlocksByType(type)) {
          const variable = block.getFieldValue('VAR');
          if (variable === before) {
            block.setFieldValue(after, 'VAR');
          }
        }
      }
      this._blockXml = Blockly.Xml.workspaceToDom(ws).outerHTML;
    });
  }

  renameMember(className, before, after) {
    const scopedBefore = `${className}::${before}`;
    const scopedAfter = `${className}::${after}`;
    return this._inNewWorkspace(ws => {
      for (const type of ['object_member', 'object_member_set', 'object_member_change', 'object_this_member', 'object_this_member_set', 'object_this_member_change']) {
        for (const block of ws.getBlocksByType(type)) {
          const variable = block.getFieldValue('MEMBER');
          if (variable === scopedBefore) {
            block.setFieldValue(scopedAfter, 'MEMBER');
          }
        }
      }
      this._blockXml = Blockly.Xml.workspaceToDom(ws).outerHTML;
    });
  }

  generateSource(useFunction = true) {
    return this._inNewWorkspace(ws => {
      for (const type of ['global_variable', 'global_variable_set', 'global_variable_change']) {
        for (const block of ws.getBlocksByType(type)) {
          const variable = block.getFieldValue('VAR');
          if (!window.editor.globalScope.variables.includes(variable)) {
            window.editor._createVariable('$global', variable, false);
          }
        }
      }

      Blockly.JavaScript.init(ws);
      const startBlock = ws.getBlocksByType('function_entry')[0];
      const preamble = startBlock.getPreamble(useFunction);
      const vmap = ws.getVariableMap();
      const locals = vmap ? vmap.getAllVariables().map(v => v.name) : [];
      const localInit = locals.length > 0 ? `let ${locals.join(', ')};\n\n` : '';
      const dbg = ''; // `console.log('${this.className}::${this.name}', ${startBlock.getArgumentNames().join(', ')});\n`;
      const body = Blockly.JavaScript.prefixLines(localInit + dbg + Blockly.JavaScript.blockToCode(startBlock).trim(), '  ');
      return `${preamble} {\n${body}\n}\n`;
    });
  }

  compile() {
    const factory = new Function(this.generateSource(true) + `return ${this.name};`);
    const fn = factory();
    fn.isUserFunction = true;
    this.compiled = fn;
    return fn;
  }
};
