import assetlist from './assetlist.js';
import AssetStore from './html52d/AssetStore.js';
import Engine, { Input } from './html52d/Engine.js';
import TouchControls from './html52d/TouchControls.js';
import Camera from './html52d/Camera.js';
import Scene from './html52d/Scene.js';
import { PIXELS_PER_UNIT } from './html52d/Point.js';
import config from './config.js';
import demos from './demos.js';
/* eslint-disable no-console */

const assets = window.assets = new AssetStore(assetlist);

function benchmark(name, iter, fn) {
  const start = performance.now();
  for(let i = 0; i < iter; i++) {
    fn(i);
  }
  const end = performance.now();
  const duration = end - start;
  const perIter = duration / iter;
  const fps = 1000.0 / perIter;
  if (perIter < 1.0) {
    console.log(name + ':', duration / iter * 1000, 'us per iteration');
  } else {
    console.log(name + ':', duration / iter, 'ms per iteration (' + fps + ' fps)');
  }
}

const engine = new Engine({
  scene: new Scene(),
  showFps: true,
  eventSource: document.getElementById('camera'),
});
window.engine = engine;
const scene = engine.activeScene;
window.scene = scene;
const sprites = window.sprites = [];

const camera = window.camera = new Camera(document.getElementById('camera'), null, null, 2);
camera.setXY(10, 7.5);
camera.layers[0].font = '16px sans-serif';
camera.layers[0].textAlign = 'center';
camera.layers[0].textBaseline = 'middle';
camera.layers[0].canvas.style.zOrder = 3;

camera.layers[1].canvas.style.zOrder = 2;
camera.layers[1].autoClear = false;
engine.cameras.push(camera);

const touchControls = new TouchControls(document.getElementById('dpad'), document.getElementById('buttons'), document.getElementById('pauseContainer'), [{ label: 'A', key: ' ' }]);
touchControls.onPauseClicked = () => pauseCheck.click();

const pauseCheck = document.getElementById('pause');
window.pauseCheck = pauseCheck;
pauseCheck.onclick = () => engine.pause(!pauseCheck.checked);
window.addEventListener('keydown', e => ((Input.normalize[e.key] || e.key) === 'Escape') && pauseCheck.click());

engine.addEventListener('enginepause', () => pauseCheck.checked = true);
engine.addEventListener('enginestart', () => pauseCheck.checked = false);
camera.addEventListener('cameramousedown', (event) => console.log(scene.spritesAt(event.detail.worldX, event.detail.worldY)))

window.commonRender = function commonRender(camera, fill = false) {
  const layer = camera.layers[this.layer];
  const pixelRect = this.pixelRect;
  // const pixelRect = new IntRect((this.origin[0] + this.hitbox[0]) * PIXELS_PER_UNIT, (this.origin[1] + this.hitbox[1]) * PIXELS_PER_UNIT, (this.origin[0] + this.hitbox[2]) * PIXELS_PER_UNIT, (this.origin[1] - this.hitbox[3]) * PIXELS_PER_UNIT);
  const style = this.currentAnimationName === 'default' ? 'black' : this.currentAnimationName;

  layer.strokeStyle = style;
  layer.beginPath();
  layer.rect(
    pixelRect[0] - .5,
    pixelRect[1] - .5,
    pixelRect[2] - pixelRect[0],
    pixelRect[3] - pixelRect[1],
  );
  layer.stroke();
  if (fill) {
    layer.fill();
  }
  layer.fillStyle = style;
  layer.fillText(this.index, pixelRect[0] + .5 * PIXELS_PER_UNIT, pixelRect[1] + .5 * PIXELS_PER_UNIT);
};

function runDemo(demo) {
  const constructOne = demo();
  if (!config.noAutoStart) {
    config.running = true;
    benchmark('construct', config.TEST_SPRITES, (i) => {
      sprites.push(constructOne(i));
    });
    benchmark('add', config.TEST_SPRITES, (i) => {
      scene.add(sprites[i]);
    });

    if (!config.TEST_COUNT) {
      // If the test count isn't overridden by the demo or the user, default to 1000
      config.TEST_COUNT = 1000;
    }
    benchmark('update', config.TEST_COUNT, () => {
      engine.step();
    });
    engine.start();
  }
}

assets.addEventListener('loadcomplete', () => {
  let demo = demos[config.DEMO_NAME];
  if (!demo) {
    config.DEMO_NAME = 'gas';
    demo = demos[config.DEMO_NAME];
  }
  console.log('Running demo:', config.DEMO_NAME);
  runDemo(demo);
}, { once: true });
