import config from './config.js';
import Sprite, { AnimationSequence, AnimationFrame } from './html52d/Sprite.js';
/* global commonRender, camera */

export default () => {
  if (!config.TEST_SPRITES) {
    config.TEST_SPRITES = 50;
  }

  const prefab = {
    animateHitboxes: true,
    animations: {
      red: new AnimationSequence([
        new AnimationFrame(null, 0, 0, 32, 32),
      ]),
      blue: new AnimationSequence([
        new AnimationFrame(null, 0, 0, 32, 32),
      ]),
      purple: new AnimationSequence([
        new AnimationFrame(null, 0, 0, 32, 32),
      ]),
    },
    defaultAnimationName: 'purple',
    start(/*scene*/) {
      this.dx = (Math.random() - 0.5) * 0.007;
      this.dy = (Math.random() - 0.5) * 0.007;
    },
    update(scene, ms) {
      if (config.running) {
        if (this.index === 0) {
          camera.rotation += ms * 0.0005;
        }
        this.teleported = false;
        this.move(this.dx * ms, this.dy * ms);
        if (this.origin[0] > 22) {
          this._moved = false;
          this.origin[0] -= 24;
          this.teleported = true;
        } else if (this.origin[0] < -2) {
          this._moved = false;
          this.origin[0] += 24;
          this.teleported = true;
        }
        if (this.origin[1] > 18) {
          this._moved = false;
          this.origin[1] -= 20;
          this.teleported = true;
        } else if (this.origin[1] < -2) {
          this._moved = false;
          this.origin[1] += 20;
          this.teleported = true;
        }
      }
    },
    onCollisionEnter(other, coll) {
      if (this.teleported || other.teleported) {
        return;
      }
      // TODO: Refactor this into a Rigidbody subclass of Sprite
      const proportion = coll.speeds[0] / coll.speed;
      this.origin[0] -= coll.penetration[0] * proportion;
      this.origin[1] -= coll.penetration[1] * proportion;
      let t;
      if (this.index < other.index) {
        if (coll.normal[0]) {
          t = other.dx;
          other.dx = this.dx;
          this.dx = t;
        }
        if (coll.normal[1]) {
          t = other.dy;
          other.dy = this.dy;
          this.dy = t;
        }
      }
    },
    onCollisionStay(other, coll) {
      const px2 = coll.penetration[0] * coll.penetration[0];
      const py2 = coll.penetration[1] * coll.penetration[1];
      if (px2 < py2) {
        this.origin[0] -= coll.penetration[0] * .55;
      } else {
        this.origin[1] -= coll.penetration[1] * .55;
      }
    },
    render: commonRender,
  };
  prefab.animations.red.frames[0].hitboxes[0].bits = 0x01;
  prefab.animations.blue.frames[0].hitboxes[0].bits = 0x02;
  prefab.animations.purple.frames[0].hitboxes[0].bits = 0x03;

  return function constructOne(index) {
    const sprite = new Sprite(prefab);
    sprite.index = index;
    sprite.origin.setXY((index % 8) * 2.0 + 1.5, Math.floor(index / 8) * 2.0 + 2.0);
    switch(sprite.index % 3) {
      case 0:
        sprite.setAnimation('red');
        break;
      case 1:
        sprite.setAnimation('blue');
        break;
      default:
        sprite.setAnimation('purple');
    }
    return sprite;
  };
};
