# html52d

html52d is a lightweight, high-performance library for building browser-based 2D games.

It is built on standard HTML5 and ECMAScript features supported by all major browsers.
Keyboard, touchscreen, and mouse inputs are all supported, including a customizable
virtual gamepad for touchscreen control. Graphics can be rendered using tilemaps,
still or animated sprites, and canvas-based vector graphics.

Since 2D games often don't want to imitate realistic physics, html52d does not provide
a full rigidbody physics implementation. Instead, it offers a simple collision detection
system using a spatial hash map to efficiently track thousands of objects even on modest
hardware. Developers are free to use other libraries such as the excellent Box2D when
such features are desired.


## Dependencies

html52d is self-contained, having no runtime dependencies in the browser or on the
web server.

html52d projects run directly in the browser without requiring a separate compilation
phase, so it has no compile-time dependencies.

GNU Make and git are required to automatically create a new project. The `make server`
command uses Python to provide a simple HTTP test server, but any web server is supported.


## Getting started

To create a new html52d-based project, issue the following command from within the
html52d source tree:
```
make project PROJECT=/path/to/project
```
This will create a new project at the specified path and initialize a git repository there.


## Creating a project manually

To create a new html52d-based project without using the automated process above:

* Create a directory for the project.
* Copy the `html52d` directory from the `html52d` distribution into the project directory.
* Copy the `.css` files from the `html52d` distribution into the project directory.
* Copy the contents of the `template` directory into the project directory.


## License

html52d is copyright (c) 2017-2023 Adam Higerd.

html52d is distributed under the MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

### Assets used in demos

[Generic Platformer Tileset](https://opengameart.org/content/generic-platformer-tileset-16x16-background)
by etqws3, used under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

[Classic Hero](https://opengameart.org/content/classic-hero) sprite by GrafxKid,
used under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
