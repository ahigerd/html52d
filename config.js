export function getQueryParam(key, def = '') {
  return decodeURIComponent((location.search.match(new RegExp('[?&]' + key + '=([^&]*)')) || [null, def])[1]);
}

export default {
  DEMO_NAME: getQueryParam('demo'),
  TEST_COUNT: getQueryParam('count', '0')|0,
  TEST_SPRITES: getQueryParam('sprites', '0')|0,
  noAutoStart: false,
  running: false,
};
