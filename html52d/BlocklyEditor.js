import * as html52d from './index.js';
import Sprite from './Sprite.js';
import EventTarget from './EventTarget.js';
import BlocklyWorkspace from '../editor/BlocklyWorkspace.js';
import BlocklyStore from '../editor/BlocklyStore.js';
import UncompressedZip from '../editor/UncompressedZip.js';
import AppearancePanel from '../editor/AppearancePanel.js';
import FunctionDef from '../editor/FunctionDef.js';
import eventTypes from '../blocks/eventTypes.js';
import { buildUI } from './UITools.js';
import '../blocks/global.js';
import '../blocks/input.js';
import '../blocks/math.js';
import '../blocks/object.js';
import '../blocks/prefab.js';
import '../blocks/sprite.js';
import '../blocks/function.js';
// import { Input } from './Engine.js';
/* global Blockly:false, scene:false, engine: false, camera: false */

// Because generated code can't have imports, copy the core exports into the global scope.
// This is only necessary for execution in the editor. Generated source files will include imports.
Object.assign(window, html52d);

const stockFiles = [
  'editor/Makefile.export',
  'editor/template.html',
  'build/build.mjs',
  'build/package.json',
  'style_demo.css',
  'AssetStore.js',
  'Camera.js',
  'Engine.js',
  'EventTarget.js',
  'Point.js',
  'Rect.js',
  'Scene.js',
  'Sprite.js',
];

const stockRename = {
  'editor/template.html': 'index.html',
  'editor/Makefile.export': 'Makefile',
};

const defaultXml = {
  '()': `<xml><block type="function_entry"><mutation name="$NAME$" />
    <statement name="PARAMS"><shadow type="function_param" /></statement>
  </block></xml>`,
  'run': `<xml><variables><variable type="" id="hero">hero</variable></variables>
    <block type="function_entry"><mutation event="run" />
      <next><block type="variables_set">
        <field name="VAR" variabletype="" id="hero">hero</field>
        <value name="VALUE"><block type="prefab_new">
          <field name="PREFAB">Hero</field>
          <value name="ORIGIN"><block type="math_point_xy"><field name="P_X">0</field><field name="P_Y">0</field></block></value>
        </block></value>
        <next><block type="prefab_add_to_scene">
          <value name="SPRITE"><block type="variables_get"><field name="VAR" id="hero" variabletype="">hero</field></block></value>
        </block></next>
      </block></next>
    </block>
  </xml>`,
  'update': `<xml><block type="function_entry"><mutation event="update" />
    <next><block type="sprite_move_speed" id="(b+yNfx#b#,qOAflPj!y">
      <value name="P_X"><block type="function_call"><mutation name="random_distance"></mutation></block></value>
      <value name="P_Y"><block type="function_call"><mutation name="random_distance"></mutation></block></value>
    </block></next></block></xml>`,
  'random_distance()': `<xml><block type="function_entry"><mutation name="random_distance" />
    <statement name="PARAMS"><shadow type="function_param" /></statement>
    <next><block type="function_return">
      <value name="VALUE"><block type="math_arithmetic"><field name="OP">MULTIPLY</field>
        <value name="A"><block type="math_arithmetic"><field name="OP">MINUS</field>
          <value name="A"><block type="math_random_float"></block></value>
          <value name="B"><block type="math_number"><field name="NUM">0.5</field></block></value>
        </block></value>
        <value name="B"><block type="math_number"><field name="NUM">10</field></block></value>
      </block></value>
    </block></next></block></xml>`,
};

const mainStubTop = `import assetlist from './assetlist';
import AssetStore from './AssetStore';
import Engine from './Engine';
import Camera from './Camera';
import Point from './Point';

const assets = window.assets = new AssetStore(assetlist);
const engine = window.engine = new Engine();
const scene = window.scene = engine.activeScene;
const camera = window.camera = new Camera(document.getElementById('camera'), null, null, 2);
engine.cameras.push(camera);

function createScene() {
`;

const mainStubBottom = `
  engine.start();
  run();
}

assets.addEventListener('load', createScene);
`;

export function makeBlocklyPrefabClass(name) {
  function wrap(cls, evt, ...args) {
    const fn = cls.functions[evt];
    if (!fn) {
      return;
    }
    if (!fn.compiled) {
      fn.compile();
    }
    fn.compiled.apply(this, args);
  }
  const eventNames = Object.values(eventTypes).filter(t => t.prefab).map(t => t.name);
  const prefabEventWrappers = eventNames.map(evt => `${evt}(...args) { wrap.call(this, ${name}, '${evt}', ...args); }`);
  const prefabEventConfig = eventNames.map(evt => `${evt}: undefined,`);
  // Do not memoize config in the constructor, since it will be dynamically changing due to the editor.
  const prefabSource = `return class ${name} extends Sprite {
    constructor(origin) {
      super(Sprite.prepareConfig({
        ...${name}.config,
        ${prefabEventConfig.join('\n        ')}
      }, false), origin);
    }

    ${prefabEventWrappers.join('\n    ')}
  }`;
  const BlocklyPrefab = new Function('Sprite', 'wrap', prefabSource)(Sprite, wrap);
  BlocklyPrefab.scope = name;
  BlocklyPrefab.config = {};
  BlocklyPrefab.functions = {};
  BlocklyPrefab.variables = [];
  return BlocklyPrefab;
}

export default class BlocklyEditor extends EventTarget {
  constructor(assets, options = {}) {
    super();
    window.editor = this;
    this.sceneScope = {
      scope: '',
      config: {},
      functions: {},
      variables: [],
    };
    this.globalScope = {
      scope: '$global',
      functions: {},
      variables: [],
    };
    window.sceneScope = this.sceneScope;

    this._ui = null;
    this._assets = assets;
    this._updateEventList = this._updateEventList.bind(this);
    this._loadEventIntoWorkspace = this._loadEventIntoWorkspace.bind(this);
    this._onWorkspaceChange = this._onWorkspaceChange.bind(this);
    this.options = options;

    this.workspace = new BlocklyWorkspace(this.options);
    window.workspace = this.workspace;

    this.workspace.registerToolboxCategory('PREFABS');
    this.workspace.addBlockToToolbox('PREFABS', 'prefab_new');
    this.workspace.addBlockToToolbox('PREFABS', 'prefab_isinstance');
    this.workspace.addBlockToToolbox('PREFABS', 'prefab_add_to_scene');
    this.workspace.addBlockToToolbox('PREFABS', 'sprite_origin');
    this.workspace.addBlockToToolbox('PREFABS', 'sprite_origin_x');
    this.workspace.addBlockToToolbox('PREFABS', 'sprite_origin_y');
    this.workspace.addBlockToToolbox('PREFABS', 'sprite_setxy');
    this.workspace.addBlockToToolbox('PREFABS', 'sprite_move');
    this.workspace.addBlockToToolbox('PREFABS', 'sprite_move_speed');
    this.workspace.addBlockToToolbox('PREFABS', 'sprite_set_animation');
    this.workspace.addBlockToToolbox('PREFABS', 'sprite_play_animation');
    this.workspace.registerToolboxCategory('INPUT');
    this.workspace.addBlockToToolbox('INPUT', 'input_key_down');
    this.workspace.registerToolboxCategory('GLOBALS');
    this.workspace.addButtonToToolbox('GLOBALS', 'Create variable...', this._createVariablePrompt.bind(this, '$global'));
    this.workspace.addBlockToToolbox('GLOBALS', 'global_variable_set');
    this.workspace.addBlockToToolbox('GLOBALS', 'global_variable_change');
    this.workspace.addBlockToToolbox('GLOBALS', 'global_variable');
    this.workspace.registerToolboxCategory('MEMBERS');
    this._createMemberButtonId = this.workspace.addButtonToToolbox('MEMBERS', 'Create variable...', this._createVariablePrompt.bind(this, null));
    this.workspace.addBlockToToolbox('MEMBERS', 'object_this_member_set');
    this.workspace.addBlockToToolbox('MEMBERS', 'object_this_member_change');
    this.workspace.addBlockToToolbox('MEMBERS', 'object_this_member');
    this.workspace.registerToolboxCategory('FUNCTIONS');
    this.workspace.addButtonToToolbox('FUNCTIONS', 'Create function...', this._createFunction.bind(this));
    this.workspace.addBlockToToolbox('FUNCTIONS', 'function_param');
    this.workspace.addBlockToToolbox('FUNCTIONS', 'function_return');
    this.workspace.addBlockToToolbox('FUNCTIONS', 'function_call');
    this.workspace.registerToolboxCategory('OBJECTS');
    this.workspace.addBlockToToolbox('OBJECTS', 'object_this');
    this.workspace.addBlockToToolbox('OBJECTS', 'object_with');
    this.workspace.addBlockToToolbox('OBJECTS', 'object_member_set');
    this.workspace.addBlockToToolbox('OBJECTS', 'object_member_change');
    this.workspace.addBlockToToolbox('OBJECTS', 'object_member');

    this.container = document.createElement('DIV');
    fetch('editor/dom_BlocklyEditor.html')
      .then(response => response.text())
      .then(html => {
        this._ui = buildUI(this, this.container, html, { AppearancePanel });
        if (this.container.parentElement) {
          const oldContainer = this.container;
          this.container = this.container.parentElement.insertBefore(this.container.firstChild, oldContainer);
          oldContainer.parentElement.removeChild(oldContainer);
        } else {
          this.container = this.container.firstChild;
        }
        return this.workspace.inject(this._ui.workspace);
      })
      .then(() => this._loadStore())
      .then(() => {
        this._objectToolbox = Blockly.Xml.textToDom(this.workspace.toolboxXml);
        this._functionToolbox = this._objectToolbox.cloneNode(true);
        const membersNode = this._functionToolbox.querySelector('category[custom="MEMBERS"]');
        membersNode.parentElement.removeChild(membersNode);

        Blockly.BlockSvg.START_HAT = true;
        this._fillSelector();
        this._ensurePrefabBlocks();
        this._updateEventList();
        this.container.classList.remove('hidden');
        this.dispatchEvent(new UIEvent('load'));
        Blockly.prompt = (message, defaultValue, callback) => this.prompt(message, defaultValue).then(callback, () => callback(null));
        Blockly.alert = (message, callback) => this.alert(message).then(callback, callback);
        Blockly.confirm = (message, callback) => this.confirm(message).then(callback, callback);
        window.realAlert = window.alert;
        window.alert = Blockly.alert;
        this.workspace.addEventListener('workspacechange', this._onWorkspaceChange);
      });

    const styleTag = document.createElement('LINK');
    styleTag.setAttribute('rel', 'stylesheet');
    styleTag.setAttribute('href', 'style_BlocklyEditor.css');
    document.head.appendChild(styleTag);

    const codeMirrorTag = document.createElement('SCRIPT');
    codeMirrorTag.setAttribute('src', 'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.45.0/codemirror.js');
    codeMirrorTag.addEventListener('load', this._codeMirrorLoaded);
    document.head.appendChild(codeMirrorTag);
  }

  get selectedFunction() {
    if (this._ui.selectPrefab.value.endsWith('()')) {
      return this._ui.selectPrefab.value.replace('()', '');
    }
    return null;
  }

  get selectedDefinition() {
    const fn = this.selectedFunction;
    if (fn) {
      return this.globalScope.functions[fn];
    }
    const scope = this._getScope(this.selectedPrefab);
    return scope.functions[this.selectedEvent];
  }

  get selectedPrefab() {
    return this._ui.selectPrefab.value;
  }

  set selectedPrefab(value) {
    if (this._ui.selectPrefab.value != value) {
      this._ui.selectPrefab.value = value || '';
      this._updateEventList();
      this._loadEventIntoWorkspace();
    }
  }

  get selectedEvent() {
    return this._ui.selectEvent.value;
  }

  set selectedEvent(value) {
    if (this._ui.selectEvent.value != value) {
      this._ui.selectEvent.value = value || '';
      this._loadEventIntoWorkspace();
    }
  }

  appendTo(container) {
    container.appendChild(this.container);
  }

  _getAllFunctions() {
    const fns = [
      ...Object.values(this.globalScope.functions),
      ...Object.values(this.sceneScope.functions),
    ];
    for (const prefab of Object.values(this._assets.prefabs)) {
      fns.push(...Object.values(prefab.functions));
    }
    return fns;
  }

  _loadStore() {
    window.bstore = this._store = new BlocklyStore();
    return this._store.getFunctions()
      .then(fns => fns.forEach(fn => {
        if (fn.className === null) {
          this.globalScope.functions[fn.name] = fn;
        } else if (fn.className === '') {
          this.sceneScope.functions[fn.name] = fn;
        } else {
          if (!this._assets.prefabs[fn.className]) {
            this._assets.prefabs[fn.className] = makeBlocklyPrefabClass(fn.className);
          }
          this._assets.prefabs[fn.className].functions[fn.name] = fn;
        }
      }))
      .then(() => this._store.getAllVariables())
      .then(vars => (vars ? Object.keys(vars) : []).forEach(prefabId => {
        if (prefabId === '$global') {
          this.workspace.ensureVariables(vars[prefabId]);
        } else if (!prefabId) {
          this.sceneScope.variables = vars[prefabId];
        } else if (this._assets.prefabs[prefabId]) {
          this._assets.prefabs[prefabId].variables = vars[prefabId];
        }
      }))
      .then(() => {
        // XXX: For demo use only
        if (!this.globalScope.functions.random_distance) {
          this.globalScope.functions.random_distance = new FunctionDef('random_distance');
          this.globalScope.functions.random_distance.blockXml = defaultXml['random_distance()'];
        }
      })
      .then(() => this._compileAll());
  }


  _fillSelector() {
    const prefabOptions = {
      'Scene': '',
      'Prefab': [],
      'Function': [],
    };
    if (this.options.allowNewPrefabs) {
      prefabOptions['Create'] = [
        { label: 'New Prefab', value: '_newPrefab' },
        { label: 'New Function', value: '_createFunction' },
      ];
    }
    this._ui.selectPrefab.setOptions(prefabOptions);
  }

  _updateEventList() {
    const prefabId = this.selectedPrefab;
    if (prefabId === '_createFunction') {
      return this._createFunction()
        .catch(() => this.selectedPrefab = '');
    }
    if (prefabId === '_newPrefab') {
      return this.prompt('Enter name of new prefab:').then(name =>
        this.selectedPrefab = this._createPrefab(name)
      ).catch(msg =>
        (msg ? this.alert(msg) : Promise.resolve())
          .then(() => this.selectedPrefab = '')
      );
    }
    if (this.selectedFunction) {
      // it's a function
      this._ui.selectEventSpan.style.display = 'none';
      this._loadEventIntoWorkspace();
      return;
    }
    this._ui.selectEventSpan.style.display = '';
    const before = this._ui.selectEvent.value;
    this._ui.selectEvent.clear();
    if (prefabId) {
      // a blank prefabId means the scene instead of a prefab
      this._ui.selectEvent.addOption('Appearance', '_appearance');
    }
    for (const evt of Object.values(eventTypes).filter(t => !t.prefab == !prefabId)) {
      this._ui.selectEvent.addOption('Event: ' + evt.name, evt.name);
    }
    if ([...this._ui.selectEvent.options].find(o => o.value === before)) {
      this._ui.selectEvent.value = before;
    } else {
      this._ui.selectEvent.value = this._ui.selectEvent.options[0].value;
    }
    this._loadEventIntoWorkspace();
  }

  _getScope(name) {
    if (name === '$global') {
      return this.globalScope;
    } else if (!name) {
      return this.sceneScope;
    } else {
      return this._assets.prefabs[name];
    }
  }

  _validateName(name, typeName, scopeOrExisting) {
    if (!name) {
      throw `A ${typeName} must have a name.`;
    }
    name = name.replace(/ /g, '_');
    if (name.match(/[^A-Za-z0-9_]/)) {
      throw 'Names may not include characters other than letters, numbers, and _.';
    }
    if (name[0] == '_') {
      throw 'Names starting with _ are reserved for the engine.';
    }
    if (Array.isArray(scopeOrExisting) && scopeOrExisting.includes(name)) {
      throw `A ${typeName} named '${name}' already exists.`;
    }
    const isGlobal = (scopeOrExisting === this.globalScope);
    const variableType = isGlobal ? 'global variable' : 'member variable';
    if (scopeOrExisting.variables && scopeOrExisting.variables.includes(name)) {
      throw `A ${variableType} named '${name}' already exists.`;
    }
    if (isGlobal && this.globalScope.functions[name]) {
      throw `A function named '${name}' already exists.`;
    }
    if (isGlobal && name in window) {
      const isUserFunction = window[name] && window[name].isUserFunction;
      const isUserVariable = this.globalScope.variables.includes(name);
      if (!isUserFunction && !isUserVariable) {
        throw `The name '${name}' is reserved for the engine.`;
      }
    }
    return name;
  }

  _createPrefab(name) {
    name = this._validateName(name, 'prefab', Object.keys(this._assets.prefabs));
    this._assets.prefabs[name] = makeBlocklyPrefabClass(name);
    this._ensurePrefabBlocks();
    return name;
  }

  _loadEventIntoWorkspace() {
    Blockly.hideChaff();
    const functionName = this.selectedFunction;
    if (!functionName && this.selectedEvent == '_appearance') {
      this._ui.workspace.style.display = 'none';
      this._ui.selectLanguageSpan.style.display = 'none';
      this._ui.jsPreview.style.display = 'none';
      this._ui.jsEditor.style.display = 'none';
      this._ui.appearancePanel.show();
      this._ui.appearancePanel.load(this._assets.prefabs[this.selectedPrefab]);
      return;
    }

    if (this.selectedPrefab && !this.selectedFunction) {
      this.workspace.updateToolbox(this._objectToolbox);
    } else {
      this.workspace.updateToolbox(this._functionToolbox);
    }
    if (this.selectedEvent === 'update' || functionName) {
      this.workspace.addBlockToToolbox('PREFABS', 'sprite_move_speed');
    } else {
      this.workspace.removeBlockFromToolbox('PREFABS', 'sprite_move_speed');
    }
    this.workspace.setButtonEnabled(this._createMemberButtonId, !functionName && this.selectedPrefab !== '');

    this._ui.selectLanguageSpan.style.display = '';
    this._ui.appearancePanel.hide();
    this._ui.workspace.style.display = '';

    let fn = this.selectedDefinition;
    if (!fn) {
      const scope = this._getScope(this.selectedPrefab);
      fn = scope.functions[this.selectedEvent] = new FunctionDef(this.selectedEvent, this.selectedPrefab);
      fn.blockXml = defaultXml[this.selectedEvent] || `<xml><block type="function_entry"><mutation event="${this.selectedEvent}" /></block></xml>`;
    }
    const xml = Blockly.Xml.textToDom(fn.blockXml);
    this.workspace.loadXml(xml);

    if (this._ui.selectLanguage.value === 'javascript') {
      this._ui.workspace.style.display = 'none';
      this._ui.jsPreview.style.display = '';
      this._ui.jsEditor.style.display = 'none';
      this._ui.jsPreviewCode.innerText = fn.generateSource(this.selectedPrefab && !this.selectedFunction);
    } else {
      this._ui.jsPreview.style.display = 'none';
      this._ui.jsEditor.style.display = 'none';
    }
  }

  _ensurePrefabBlocks() {
    const prefabOptions = [...this._ui.selectPrefab.options].map(o => o.value);
    for (const key of Object.keys(this._assets.prefabs)) {
      if (!prefabOptions.includes(key)) {
        this._ui.selectPrefab.addOption(key, key, 'Prefab');
      }
    }
    for (const name of Object.keys(this.globalScope.functions)) {
      const key = name + '()';
      if (!prefabOptions.includes(key)) {
        this._ui.selectPrefab.addOption(name, key, 'Function');
      }
    }
  }

  _onWorkspaceChange() {
    const fn = this.selectedDefinition;
    if (!fn) {
      return;
    }
    fn.blockXml = this.workspace.getXml();
    this._store.saveFunction(fn);
    fn.compile();
  }

  _pauseWhile(fn) {
    if (!engine.running) {
      return Promise.resolve(fn());
    }
    engine.pause(false);
    const resume = result => new Promise(resolve => window.setTimeout(() => {
      camera.container.focus();
      engine.pause(true);
      resolve(result);
    }, 200));
    return Promise.resolve(fn()).then(resume, err => Promise.reject(resume(err)));
  }

  // Subclass BlocklyEditor to override this with a custom UI.
  prompt(message, initial = '') {
    return this._pauseWhile(() => new Promise((resolve, reject) => {
      const result = window.prompt(message, initial);
      if (result === null) {
        reject();
      } else {
        resolve(result);
      }
    }));
  }

  // Subclass BlocklyEditor to override this with a custom UI.
  alert(message) {
    return this._pauseWhile(() => window.realAlert(message));
  }

  // Subclass BlocklyEditor to override this with a custom UI.
  confirm(message) {
    return this._pauseWhile(() => window.confirm(message));
  }

  getXml(prefabId, eventName) {
    if (prefabId.endsWith('()')) {
      return this.globalScope.blockXml[prefabId] || defaultXml[prefabId] || defaultXml['()'] || '';
    }
    const obj = this._getScope(prefabId);
    if (obj && obj.blockXml[eventName]) {
      return obj.blockXml[eventName];
    } else if (defaultXml[eventName]) {
      return defaultXml[eventName];
    } else {
      return `<xml><block type="function_entry"><mutation event="${eventName}" /></block></xml>`;
    }
  }

  _compileAll() {
    for (const prefab of Object.values(this._assets.prefabs)) {
      for (const fn of Object.values(prefab.functions)) {
        fn.compile();
      }
    }
    for (const fn of Object.values(this.sceneScope.functions)) {
      window[fn.name] = fn.compile();
    }
    for (const fn of Object.values(this.globalScope.functions)) {
      window[fn.name] = fn.compile();
    }
  }

  run() {
    engine.pause(false);
    for (let i = scene.objects.length - 1; i >= 0; --i) {
      scene.remove(scene.objects[i]);
    }

    this._compileAll();

    for (const variable of this.sceneScope.variables) {
      window[variable] = null;
    }
    camera.container.focus();
    engine.start();
    this.sceneScope.functions.run.compiled();
  }

  _generateAssetList() {
    // TODO: other assets
    const assetlist = {
      prefabs: {},
    };
    for (const prefabId of Object.keys(this._assets.prefabs)) {
      assetlist.prefabs[prefabId] = `prefabs/${prefabId}.js`;
    }
    return 'export default ' + JSON.stringify(assetlist, null, '  ') + ';\n';
  }

  _importFunctions(path = '.') {
    const fns = Object.values(this.globalScope.functions);
    if (!fns.length) {
      return '';
    }
    return `import { ${fns.map(fn => fn.name).join(', ')} } from '${path}/functions.js';\n`;
  }

  _generateCodeForGlobals(zip) {
    const fns = Object.values(this.globalScope.functions);
    if (!fns.length) {
      return '';
    }
    let exportJs = '';
    for (const fn of fns) {
      if (zip) {
        zip.addFile(`xml/_${fn.name}.xml`, fn.blockXml);
      }
      const source = fn.generateSource(true);
      exportJs += `${source}\n`;
    }
    exportJs += `export { ${fns.map(fn => fn.name).join(', ')} };`;
    return exportJs;
  }

  _generateCodeForScene(zip) {
    const fnDefs = [];
    for (const fn of Object.values(this.sceneScope.functions)) {
      if (zip) {
        zip.addFile(`xml/scene_${fn.name}.xml`, fn.blockXml);
      }
      fnDefs.push(fn.generateSource(true));
    }
    const sceneJs = Blockly.JavaScript.prefixLines(fnDefs.join('\n'), '  ');
    return this._importFunctions() + mainStubTop + sceneJs + mainStubBottom;
  }

  _generateCodeForPrefab(prefabId, zip) {
    const prefab = this._assets.prefabs[prefabId];
    const fnDefs = [];
    for (const fn of Object.values(prefab.functions)) {
      if (zip) {
        zip.addFile(`xml/${prefabId}_${fn.name}.xml`, fn.blockXml);
      }
      fnDefs.push(Blockly.JavaScript.prefixLines(fn.generateSource(false), '  '));
    }
    return this._importFunctions('..') +
      'import Sprite from \'../Sprite\';\nimport Point from \'../Point\';\n\n' +
      `export default class ${prefabId} extends Sprite {\n` +
      fnDefs.join('\n') +
      '};\n';
    return prefabJs;
  }

  exportSource() {
    const zip = new UncompressedZip();

    const funcJs = this._generateCodeForGlobals(zip);
    zip.addFile('functions.js', funcJs);

    const sceneJs = this._generateCodeForScene(zip);
    zip.addFile('main.js', sceneJs);

    for (const prefabId of Object.keys(this._assets.prefabs)) {
      const prefabJs = this._generateCodeForPrefab(prefabId, zip);
      zip.addFile(`prefabs/${prefabId}.js`, prefabJs);
    }

    zip.addFile('assetlist.js', this._generateAssetList());

    Promise.all(stockFiles.map(filename => fetch(`./${filename}`)))
      .then(responses => Promise.all(responses.map(response => response.text())))
      .then(scripts => {
        for (let i = 0; i < stockFiles.length; i++) {
          zip.addFile(stockRename[stockFiles[i]] || stockFiles[i], scripts[i]);
        }
      })
      .then(() => zip.toBlob())
      .then(blob => {
        const url = URL.createObjectURL(blob);
        setTimeout(() => URL.revokeObjectURL(url), 1000);
        const a = document.createElement('A');
        a.href = url;
        a.download = this.projectName ? `${this.projectName}.zip` : 'html52d-export.zip';
        a.rel = 'noopener';
        a.dispatchEvent(new MouseEvent('click'));
      });
  }

  _codeMirrorLoaded() {
  }

  _createFunction() {
    return this.prompt('New function name:')
      .then(name => {
        name = this._validateName(name, 'function', this.globalScope);
        const key = name + '()';
        this.globalScope.blockXml[key] = defaultXml['()'].replace('$NAME$', name);
        this._ensurePrefabBlocks();
        this.selectedPrefab = key;
      })
      .catch(msg => Promise.reject(msg && this.alert(msg)));
  }

  _createVariable(scopeName, name, validate = true) {
    const scope = this._getScope(scopeName);
    if (validate) {
      name = this._validateName(name, scopeName === '$global' ? 'global variable' : 'member variable', scope);
    }
    scope.variables.push(name);
    this._store.saveVariables(scopeName, scope.variables);
    this.workspace.refreshToolbox();
  }

  _createVariablePrompt(scopeName = null) {
    if (scopeName === null) {
      scopeName = this.selectedPrefab;
    }
    return this.prompt('New variable name:')
      .then(name => this._createVariable(scopeName, name, true))
      .catch(msg => msg && this.alert(msg));
  }

  renameGlobal(before) {
    return this.prompt(`Rename global '${before}' variable to:`)
      .then(after => {
        after = this._validateName(after, 'global variable', this.globalScope);
        const pos = this.globalScope.variables.indexOf(before);
        this.globalScope.variables[pos] = after;
        for (const fn of this._getAllFunctions()) {
          fn.renameGlobal(before, after);
        }
        this._loadEventIntoWorkspace();
      })
      .catch(msg => msg && this.alert(msg));
  }

  renameMember(className, before) {
    return this.prompt(`Rename '${before}' member variable to:`)
      .then(after => {
        const scope = this._getScope(this.selectedPrefab);
        after = this._validateName(after, 'member variable', scope);
        const pos = scope.variables.indexOf(before);
        scope.variables[pos] = after;
        for (const fn of this._getAllFunctions()) {
          fn.renameMember(this.selectedPrefab, before, after);
        }
        this._loadEventIntoWorkspace();
      })
      .catch(msg => msg && this.alert(msg));
  }
}
