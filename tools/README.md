# Tools

The tools in this folder aren't part of the core html52d library. Everything here is optional and
won't be applicable to all games. As these tools are distributed under the same permissive license
as the rest of html52d, developers are freely encouraged to copy them into their projects and
modify them as they see fit.


## PlatformerPhysics

This mixin can be applied to a Sprite instance or subclass to give it common behaviors for a 2D
platformer game.

A sprite with PlatformerPhysics is affected by gravity. The sprite cannot pass through TileMap
tiles that it collides with.

To apply PlatformerPhysics to a Sprite object:
```
import Sprite from './html52d/Sprite.js';
import PlatformerPhysics from './tools/PlatformerPhysics.js';

const sprite = new Sprite(prefab);
PlatformerPhysics.mixin(sprite);
```

To apply PlatformerPhysics to a Sprite subclass:
```
import Sprite from './html52d/Sprite.js';
import PlatformerPhysics from './tools/PlatformerPhysics.js';

class Character extends Sprite {
  /* ... */
}

PlatformerPhysics.mixin(Character);
```

