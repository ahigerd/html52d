import CONFIG from '../config.js';
import createMixin from './mixin.js';

const PlatformerPhysics = {
  start() {
    this.isGrounded = false;
    this.isJumping = false;
    this.gravity = CONFIG.gravity || 0.003; // units per ms^2
    this.fallSpeed = 0;
    this._platformer = {
      lastTilemap: null,
      updateMs: 0,
    };
  },
  update(scene, ms) {
    if (this.isGrounded && this._platformer.lastTilemap) {
      if (!this._platformer.lastTilemap.computeCollision(this, true)) {
        this.isGrounded = false;
        this.fallSpeed = this.gravity * ms;
        if (this.onStartFalling) {
          this.onStartFalling();
        }
      }
    } else {
      if (!this.isJumping && this.fallSpeed < 0) {
        this.fallSpeed *= 0.5;
      }
      this.fallSpeed += this.gravity * ms;
    }
    this._platformer.updateTime = ms * .001;
    this.move(0, this.fallSpeed * this._platformer.updateTime);
  },
  startJump(speed) {
    this.move(0, this.fallSpeed * -this._platformer.updateTime);
    this.fallSpeed = speed;
    this.isGrounded = false;
    this.isJumping = true;
  },
  releaseJump() {
    this.isJumping = false;
  },
  onCollisionEnter(other, coll) {
    if (other.isTileMap) {
      PlatformerPhysics.onCollisionStay.call(this, other, coll);
    }
  },
  onCollisionStay(other, coll) {
    if (other.isTileMap) {
      this._platformer.lastTilemap = other;
      this.move(coll.penetration[0], coll.penetration[1]);
      const dy = this.fallSpeed;
      if (coll.penetration[1] < 0 && dy > 0) {
        if (!this.isGrounded && this.onLanded) {
          this.onLanded();
        }
        this.isGrounded = true;
        this.fallSpeed = 0;
      } else if (coll.penetration[1] > 0 && dy < 0) {
        this.fallSpeed = 0;
      }
    }
  },
};

export default createMixin(PlatformerPhysics);
