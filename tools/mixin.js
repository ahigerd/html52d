function mixin(prototype, obj) {
  if (typeof obj === 'function') {
    obj = obj.prototype;
  }
  for (const key in prototype) {
    const original = obj[key];
    if (original) {
      obj[key] = function(...args) {
        prototype[key].apply(this, args);
        return original.apply(this, args);
      };
    } else {
      obj[key] = prototype[key];
    }
  }
  if (prototype.start && obj.scene) {
    prototype.start.call(obj, scene);
  }
}

export default function createMixin(prototype) {
  return {
    prototype,
    mixin: mixin.bind(null, prototype),
  }
}
