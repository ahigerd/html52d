import gas_spin from './demo-gas-spin.js';
import gas from './demo-gas.js';
import platform from './demo-platform.js';
import topdown from './demo-topdown.js';
import transform from './demo-transform.js';
import trigger from './demo-trigger.js';
import blockly from './demo-blockly.js';

export default {
  'gas-spin': gas_spin,
  gas,
  platform,
  topdown,
  transform,
  trigger,
  blockly,
};
