import assetlist from './assetlist.js';
import AssetStore from './html52d/AssetStore.js';
import Engine, { Input } from './html52d/Engine.js';
import TouchControls from './html52d/TouchControls.js';
import Camera from './html52d/Camera.js';
import Scene from './html52d/Scene.js';
import config from './config.js';
/* eslint-disable no-console */

const assets = window.assets = new AssetStore(assetlist);

const engine = new Engine({
  scene: new Scene(),
  showFps: true,
  eventSource: document.getElementById('camera'),
});

const camera = new Camera(document.getElementById('camera'));
engine.cameras.push(camera);

const touchControls = new TouchControls(document.getElementById('dpad'), document.getElementById('buttons'), document.getElementById('pauseContainer'), [{ label: 'A', key: ' ' }]);
touchControls.onPauseClicked = () => pauseCheck.click();

const pauseCheck = document.getElementById('pause');
window.pauseCheck = pauseCheck;
pauseCheck.onclick = () => engine.pause(!pauseCheck.checked);
window.addEventListener('keydown', e => ((Input.normalize[e.key] || e.key) === 'Escape') && pauseCheck.click());

engine.addEventListener('enginepause', () => pauseCheck.checked = true);
engine.addEventListener('enginestart', () => pauseCheck.checked = false);

function run() {
  engine.start();
}

assets.addEventListener('loadcomplete', () => {
  run();
}, { once: true });
