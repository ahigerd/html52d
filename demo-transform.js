import config from './config.js';
import Sprite, { AnimationSequence, AnimationFrame } from './html52d/Sprite.js';
/* global camera, commonRender */

export default () => {
  if (!config.TEST_SPRITES) {
    config.TEST_SPRITES = 56;
  }

  const prefab = {
    animateHitboxes: true,
    animations: {
      default: new AnimationSequence([
        new AnimationFrame(null, 0, 0, 32, 32),
      ]),
    },
    start(/*scene*/) {
      this.d_theta = 0.0001;
      this.d_phi = 0.00035;
      this.phi = 0;
    },
    update(scene, ms) {
      if (config.running && this.index === 0) {
        camera.rotation += this.d_theta * ms;
        this.phi += this.d_phi * ms;
        camera.skewX = Math.cos(this.phi);
        camera.skewY = Math.sin(this.phi);
      }
    },
    render: commonRender,
  };

  return function constructOne(index) {
    const sprite = new Sprite(prefab);
    sprite.index = index;
    sprite.origin.setXY((index % 8) * 2.0 + 1.5, Math.floor(index / 8) * 2.0 + 2.0);
    return sprite;
  };
};
